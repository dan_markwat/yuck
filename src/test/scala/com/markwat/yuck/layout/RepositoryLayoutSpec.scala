package com.markwat.yuck.layout

import java.net.URI

import com.markwat.yuck.{PkgTarget, TestData}
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 5/23/2017.
  */
class RepositoryLayoutSpec extends FlatSpec with Matchers {

  val base = URI.create("http://localhost:8099/root/")
  val metadata = URI.create("http://localhost:8099/root/abc/1.0/metadata.json")
  val data = URI.create("http://localhost:8099/root/abc/1.0/abc-1.0.tar")

  val layout = RepositoryLayout(base)

  "The default" should "produce a metadata.json file path under the pkg target's path" in {
    layout.metadata(PkgTarget("abc", "1.0")) should equal(metadata)
  }

  it should "produce a data file based on the pkg's packaging and base path" in {
    layout.data(TestData.pkgAbc) should equal(data)
  }

  it should "have all references rooted under its base and never resolve elsewhere" in {
    layout.path(PkgTarget("http://foo.bar/foo/bar", "myver")) should equal(base.resolve("/foo/bar/myver/"))
  }
}
