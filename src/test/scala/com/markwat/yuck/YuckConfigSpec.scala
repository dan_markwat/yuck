package com.markwat.yuck

import java.io.File
import java.nio.file.Paths

import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.JavaConverters._

/**
  * Created by danie on 5/23/2017.
  */
class YuckConfigSpec extends FlatSpec with Matchers {

  "The default config" should "load all expected defaults" in {
    val config = YuckConfig()
    config.siteDir should equal(Paths.get("/var/lib/yuck"))
    config.inventoryDir should equal(Paths.get("/var/lib/yuck/inventory"))
    config.cacheDir should equal(Paths.get("/var/cache/yuck"))
    config.sources should be(empty)
  }

  "A built config" should "load all expected values" in {
    val sources = Seq("http://localhost:8000/").asJava
    val conf = ConfigFactory.empty().withValue("yuck.site", ConfigValueFactory.fromAnyRef("yuck/site"))
      .withValue("yuck.inventory", ConfigValueFactory.fromAnyRef("yuck/inventory"))
      .withValue("yuck.cache", ConfigValueFactory.fromAnyRef("yuck/cache"))
      .withValue("yuck.sources", ConfigValueFactory.fromIterable(sources))

    val config = YuckConfig(conf)

    config.siteDir should equal(Paths.get("yuck/site"))
    config.inventoryDir should equal(Paths.get("yuck/inventory"))
    config.cacheDir should equal(Paths.get("yuck/cache"))
    config.sources should contain(SourceConf(new java.net.URI("http://localhost:8000/")))
  }
}
