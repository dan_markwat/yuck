package com.markwat.yuck

import java.nio.file.Paths

import com.markwat.yuck.cache.CacheController.DirectSyncCacheController
import com.markwat.yuck.cache.PkgCache
import com.markwat.yuck.resolver.NPkgResolver
import com.markwat.yuck.source.RemoteSourcePool
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 5/23/2017.
  */
class YuckContextSpec extends FlatSpec with MockFactory with Matchers {

  trait MockBehaviors {
    val config: YuckConfig = stub[YuckConfig]
    (config.sources _).when().returns(Nil.toSet)
    (config.cacheDir _).when().returns(Paths.get("/var/cache/yuck"))
    (config.siteDir _).when().returns(Paths.get("/"))
    (config.inventoryDir _).when().returns(Paths.get("/var/lib/yuck/inventory"))

    val ctx = YuckContext(config)
  }

  "A default context" should "have default settings" in new MockBehaviors {
    ctx.remoteSources should equal(Nil.toSet)
    ctx.resolver.getClass should be(classOf[NPkgResolver])
    ctx.cacheController.getClass should be(classOf[DirectSyncCacheController])
    ctx.inventory.getClass should be(classOf[Inventory.InventoryImpl])
    ctx.pkgCache.getClass should be(classOf[PkgCache])
    ctx.sourcePool.getClass should be(classOf[RemoteSourcePool])
  }
}
