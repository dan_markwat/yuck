package com.markwat.yuck.json

import java.io.StringReader
import java.net.URI
import java.nio.charset.Charset
import java.time.Instant

import com.markwat.yuck._
import org.apache.commons.io.IOUtils
import org.apache.commons.io.input.ReaderInputStream
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.JavaConverters._

/**
  * Created by danie on 5/23/2017.
  */
class JsonSerializerSpec extends FlatSpec with Matchers {

  object Json {
    val pkg = Pkg("abc", "1.0",
      PkgRemote(new URI("http://localhost/abc/1.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("xyz", "1.0") :: Nil, "tar"))

    val pkgStr =
      """
         |{
         |  "fqn": "abc",
         |  "version": "1.0",
         |  "remote": {
         |    "location": "http://localhost/abc/1.0/",
         |    "sourceConf": {
         |      "location": "http://localhost/"
         |    }
         |  },
         |  "yuckyData": {
         |    "published": "2017-05-23T15:48:44.533Z",
         |    "buildNumber": "1",
         |    "dependencies": [
         |      {
         |        "fqn": "xyz",
         |        "version": "1.0"
         |      }
         |    ],
         |    "packaging": "tar"
         |  }
         |}
      """.stripMargin

    val pkgManifest = PkgManifest(pkg, Instant.parse("2017-05-23T15:48:44.533Z"), Seq("/abc/123", "/xyz/def"))

    val pkgManifestStr =
      """
        |{
        |  "target": {
        |    "fqn": "abc",
        |    "version": "1.0",
        |    "remote": {
        |      "location": "http://localhost/abc/1.0/",
        |      "sourceConf": {
        |        "location": "http://localhost/"
        |      }
        |    },
        |    "yuckyData": {
        |      "published": "2017-05-23T15:48:44.533Z",
        |      "buildNumber": "1",
        |      "dependencies": [
        |        {
        |          "fqn": "xyz",
        |          "version": "1.0"
        |        }
        |      ],
        |      "packaging": "tar"
        |    }
        |  },
        |  "timestamp": "2017-05-23T15:48:44.533Z",
        |  "files": [
        |    "/abc/123",
        |    "/xyz/def"
        |  ]
        |}
      """.stripMargin

    val validJson = IOUtils.readLines(this.getClass.getResourceAsStream("/random.json"), Charset.forName("UTF-8")).asScala.mkString("")
    val invalidJson = """}LKU^%$E%^&UHNM<JYTR%^YHBNML"""
  }

  "The serializer" should "serialize a Pkg" in {
    JsonSerializer.serializer(Json.pkg).close()
  }

  it should "deserialize a Pkg" in {
    JsonSerializer.deserializer[Pkg].apply(new ReaderInputStream(new StringReader(Json.pkgStr), Charset.forName("UTF-8")))
  }

  it should "serialize and deserialize losslessly" in {
    val is = JsonSerializer.serializer(Json.pkg)
    val pkg = JsonSerializer.deserializer[Pkg].apply(is)

    pkg should equal(Json.pkg)
  }

  it should "serialize a PkgManifest" in {
    JsonSerializer.serializer(Json.pkgManifest)
  }

  it should "deserialize a PkgManifest" in {
    JsonSerializer.deserializer[PkgManifest].apply(new ReaderInputStream(new StringReader(Json.pkgManifestStr), Charset.forName("UTF-8")))
  }

  it should "serialize and deserialize a PkgManifest losslessly" in {
    val is = JsonSerializer.serializer(Json.pkgManifest)
    val manifest = JsonSerializer.deserializer[PkgManifest].apply(is)

    manifest should equal(Json.pkgManifest)}

  it should "throw an exception when deserializing garbage json" in {
    an[Exception] should be thrownBy {
      JsonSerializer.deserializer[Pkg].apply(new ReaderInputStream(new StringReader(Json.invalidJson), Charset.forName("UTF-8")))
    }
  }
}
