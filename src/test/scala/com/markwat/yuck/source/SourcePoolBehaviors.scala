package com.markwat.yuck.source

import java.net.URI

import com.markwat.yuck.PkgTarget
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 6/22/2017.
  */
trait SourcePoolBehaviors {
  this: FlatSpec with Matchers =>

  trait Fixture {
    def serve: URI

    def serveNeg: URI

    def target: PkgTarget

    def targetNeg: PkgTarget
  }

  def typical(pool: => SourcePool, fixture: Fixture): Unit = {
    it should "get the correct remote source serving a URI" in {
      val rs = pool.source(fixture.serve)
      rs.serves(fixture.serve) should be(true)

      an[Exception] should be thrownBy {
        pool.source(fixture.serveNeg)
      }
    }

    it should "know when a pkg exists in its pool" in {
      pool.exists(fixture.target) should be(true)
      pool.exists(fixture.targetNeg) should be(false)
    }

    it should "obtain pkg data from the place defined by the metadata" in {
      // todo wire in the mockserver so it fails when trying to use the wrong server
      val pkg = pool.metadata(fixture.target)
      val data = pool.data(pkg)
    }
  }
}
