package com.markwat.yuck.source

import java.net.URI
import java.time.Instant

import com.markwat.yuck._
import com.markwat.yuck.json.JsonSerializer
import com.markwat.yuck.serde.Deser
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 5/17/2017.
  */
trait RemoteSourceBehaviors {
  this: FlatSpec with Matchers =>

  implicit val deserializer: Deser[Pkg] = JsonSerializer.deserializer[Pkg]

  object Pkgs {
    val exists = Pkg("abc", "1.0",
      PkgRemote(new URI("http://localhost/abc/1.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("xyz", "1.0") :: Nil, "tar"))
    val existsTarget = PkgTarget(exists)

    val notExists = Pkg("foopoo", "1.x",
      PkgRemote(new URI("http://localhost/foopoo/1.x/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", Nil, "tar"))
    val notExistsTarget = PkgTarget(notExists)
  }

  def typical(remote: => RemoteSource): Unit = {
    it should "know when something exists or not" in {
      remote.exists(Pkgs.existsTarget) should be(true)

      noException should be thrownBy {
        remote.exists(Pkgs.notExistsTarget) should be(false)
      }
    }

    it should "give valid metadata for existing pkgs" in {
      remote.metadata(Pkgs.existsTarget) should equal(Pkgs.exists)
    }

    it should "get a valid PkgData object for existing pkgs" in {
      val data = remote.data(Pkgs.exists)
      data.pkg should equal(Pkgs.exists)
      data.consume({ is => is.close() })
    }

    it should "throw an exception when it can't resolve metadata or data" in {
      an[Exception] should be thrownBy {
        remote.metadata(Pkgs.notExistsTarget)
      }

      an[Exception] should be thrownBy {
        remote.data(Pkgs.notExists).consume(is => is.close())
      }
    }
  }
}
