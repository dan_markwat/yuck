package com.markwat.yuck.source

import com.markwat.yuck.client.WebClient
import com.markwat.yuck.{Pkg, SourceConf, TestData}
import org.mockserver.client.server.MockServerClient
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 5/17/2017.
  */
class WebRemoteSourceSpec extends FlatSpec with Matchers with RemoteSourceBehaviors with WebClient {

  implicit val deser = com.markwat.yuck.json.JsonSerializer.deserializer[Pkg]

  override def expectations(mockServer: MockServerClient): Unit = {
    mockServer.when(
      request().withMethod("GET").withPath("/abc/1.0/metadata.json")
    ).respond(
      response().withStatusCode(200).withBody(TestData.load("/testsite/abc/1.0/metadata.json"))
    )

    mockServer.when(
      request().withMethod("GET").withPath("/abc/1.0/abc-1.0.tar")
    ).respond(
      response().withStatusCode(200).withBody(TestData.load("/testsite/abc/1.0/abc-1.0.tar"))
    )

    mockServer.when(
      request().withMethod("HEAD").withPath("/abc/1.0/metadata.json")
    ).respond(
      response().withStatusCode(200)
    )
  }

  "A default source" should behave like typical(new SingleRemoteSource(SourceConf(this.host)))
}
