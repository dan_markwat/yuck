package com.markwat.yuck.graph

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 5/17/2017.
  */
class JenaPkgGraphSpec extends FlatSpec with Matchers with PkgGraphBehaviors {

  "A Jena-based pkg graph" should behave like typical(JenaPkgGraph.memory(), new Fixture {})
}
