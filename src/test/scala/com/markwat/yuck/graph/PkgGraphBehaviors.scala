package com.markwat.yuck.graph

import java.net.URI
import java.time.Instant

import com.markwat.yuck._
import org.scalatest.{FlatSpec, Matchers}
import resource.managed

/**
  * Created by danie on 6/20/2017.
  */
// todo needs more test cases
trait PkgGraphBehaviors {
  this: FlatSpec with Matchers =>

  trait Fixture {
  }

  object CompletePkgs {
    val pkgAbc = Pkg("abc", "1.0",
      PkgRemote(new URI("http://localhost/abc/1.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("xyz", "1.0") :: Nil, "tar"))

    val pkgXyz = Pkg("xyz", "1.0",
      PkgRemote(new URI("http://localhost/xyz/1.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("def", "2.0") :: Nil, "tar"))

    val pkgDef = Pkg("def", "2.0",
      PkgRemote(new URI("http://localhost/def/2.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", Nil, "tar"))
  }

  object CyclePkgs {
    val pkgAbc = Pkg("abc", "1.0",
      PkgRemote(new URI("http://localhost/abc/1.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("xyz", "1.0") :: Nil, "tar"))

    val pkgXyz = Pkg("xyz", "1.0",
      PkgRemote(new URI("http://localhost/xyz/1.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("def", "2.0") :: Nil, "tar"))

    val pkgDef = Pkg("def", "2.0",
      PkgRemote(new URI("http://localhost/def/2.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("abc", "1.0") :: Nil, "tar"))
  }

  object IncompletePkgs {
    val pkgAbc = Pkg("abc", "1.0",
      PkgRemote(new URI("http://localhost/abc/1.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("xyz", "1.0") :: Nil, "tar"))

    val pkgXyz = Pkg("xyz", "1.0",
      PkgRemote(new URI("http://localhost/xyz/1.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("def", "2.0") :: Nil, "tar"))

    val pkgDef = Pkg("def", "2.0",
      PkgRemote(new URI("http://localhost/def/2.0/"),
        SourceConf(new URI("http://localhost/"))),
      YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("missing", "1.0") :: Nil, "tar"))
  }

  def typical(pkgGraph: => PkgGraph, fixture: Fixture): Unit = {
    it should "add, and provide iterator over added, pkgs" in managed(pkgGraph).map({ graph =>
      graph.add(CompletePkgs.pkgAbc)
      graph.add(CompletePkgs.pkgXyz)
      graph.add(CompletePkgs.pkgDef)

      graph should contain allOf(
        PkgResource(CompletePkgs.pkgAbc),
        PkgResource(CompletePkgs.pkgXyz),
        PkgResource(CompletePkgs.pkgDef))
    }).tried.get

    it should "know when cycles exist" in managed(pkgGraph).map({ graph =>
      graph.add(CyclePkgs.pkgAbc)
      graph.add(CyclePkgs.pkgXyz)
      graph.add(CyclePkgs.pkgDef)

      graph.hasCycles should be(true)
    }).tried.get

    it should "not have cycles on an empty graph" in managed(pkgGraph).map({ graph =>
      graph.hasCycles should be(false)
    }).tried.get

    it should "yield an empty iterator on an empty graph" in managed(pkgGraph).map({ graph =>
      graph should be(empty)
    }).tried.get

    it should "know when it's complete" in {
      managed(pkgGraph).map({ graph =>
        graph.add(CompletePkgs.pkgAbc)
        graph.add(CompletePkgs.pkgXyz)
        graph.add(CompletePkgs.pkgDef)

        graph.isComplete should be(true)
        graph.hasCycles should be(false)
      }).tried.get

      managed(pkgGraph).map({ graph =>
        graph.add(IncompletePkgs.pkgAbc)
        graph.add(IncompletePkgs.pkgXyz)
        graph.add(IncompletePkgs.pkgDef)

        graph.isComplete should be(false)
        graph.hasCycles should be(false)
      }).tried.get
    }

    it should "provide the sets of cycles when they exist" in managed(pkgGraph).map({ graph =>
      graph.add(CyclePkgs.pkgAbc)
      graph.add(CyclePkgs.pkgXyz)
      graph.add(CyclePkgs.pkgDef)

      graph.hasCycles should be(true)
      graph.cycles().foreach { cycle =>
        cycle should contain allOf(PkgResource(CyclePkgs.pkgAbc),
          PkgResource(CyclePkgs.pkgXyz),
          PkgResource(CyclePkgs.pkgDef))
      }
    }).tried.get

    it should "produce an empty list if no cycles exist" in managed(pkgGraph).map({ graph =>
      graph.add(CompletePkgs.pkgAbc)
      graph.add(CompletePkgs.pkgXyz)
      graph.add(CompletePkgs.pkgDef)

      graph.hasCycles should be(false)
      graph.cycles() should be(empty)
    }).tried.get

    it should "deduce a pkg graph but only if it's complete" in {
      managed(pkgGraph).map({ graph =>
        graph.add(CompletePkgs.pkgAbc)
        graph.add(CompletePkgs.pkgXyz)
        graph.add(CompletePkgs.pkgDef)

        noException should be thrownBy {
          graph.deduce()
        }
      }).tried.get

      managed(pkgGraph).map({ graph =>
        graph.add(IncompletePkgs.pkgAbc)
        graph.add(IncompletePkgs.pkgXyz)
        graph.add(IncompletePkgs.pkgDef)

        an[Exception] should be thrownBy {
          graph.deduce()
        }
      }).tried.get
    }

    it should "deduce a topologically sorted sequence of pkgs for a complete graph" in managed(pkgGraph).map({ graph =>
      graph.add(CompletePkgs.pkgAbc)
      graph.add(CompletePkgs.pkgXyz)
      graph.add(CompletePkgs.pkgDef)

      graph.deduce().ordered should equal(
        Seq(
          PkgResource(CyclePkgs.pkgDef),
          PkgResource(CyclePkgs.pkgXyz),
          PkgResource(CyclePkgs.pkgAbc)
        )
      )
    }).tried.get

    it should "deduce a tree-like structure of pkgs for a complete graph" in managed(pkgGraph).map({ graph =>
      import CompletePkgs._

      graph.add(pkgAbc)
      graph.add(pkgXyz)
      graph.add(pkgDef)

      graph.deduce().tree should equal(
        Level(Set(
          (PkgResource(pkgAbc), PkgResource(pkgXyz), 0),
          (PkgResource(pkgXyz), PkgResource(pkgDef), 1)))
      )
    }).tried.get
  }
}
