package com.markwat.yuck.graph

import com.markwat.yuck.graph.CycleDetector.Vertex
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable

/**
  * Created by danie on 5/18/2017.
  */
class CycleDetectorSpec extends FlatSpec with Matchers {

  val exampleVertices: mutable.Map[String, Vertex] = mutable.Map.empty ++
    mutable.Seq(Vertex("1", Seq("2", "4")),
      Vertex("2", Seq("3")),
      Vertex("3", Seq("1", "7")),
      Vertex("4", Seq("6")),
      Vertex("5", Seq("4")),
      Vertex("6", Seq("7")),
      Vertex("7", Seq("5")),
      Vertex("8", Seq("6", "11")),
      Vertex("9", Seq("8")),
      Vertex("10", Seq("9")),
      Vertex("11", Seq("10", "12")),
      Vertex("12", Seq("7", "10")))
      .groupBy(_.id).mapValues(_.head)

  "A directed acyclic graph with cycles" should "have its cycles found" in {
    val detector = new CycleDetector(exampleVertices)
    detector.run()
    detector.components.foreach { kv =>
      Console.println(s"Strongly connected components ${kv._1}: ${kv._2.mkString(", ")}")
    }

    detector.components.keys.toSet should equal(Set(1, 2, 3))
    detector.components(2).map(_.id).toSet should equal(Set("1", "2", "3"))
    detector.components(1).map(_.id).toSet should equal(Set("4", "5", "6", "7"))
    detector.components(3).map(_.id).toSet should equal(Set("8", "9", "10", "11", "12"))
  }
}
