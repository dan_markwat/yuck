package com.markwat.yuck.cache

import java.nio.file.{Files, Paths}
import java.util.UUID

import com.markwat.yuck.Pkg
import com.markwat.yuck.json.JsonSerializer
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 5/17/2017.
  */
class PkgCacheSpec extends FlatSpec with Matchers with MockFactory with PkgCacheBehaviors {

  implicit val ser = JsonSerializer.serializer[Pkg]
  implicit val deser = JsonSerializer.deserializer[Pkg]

  def fixture = new PkgCache({
    val path = Paths.get("target").resolve("pkgcache-" + UUID.randomUUID())
    Files.createDirectories(path)
    path.toFile.deleteOnExit()
    path
  })

  "An empty cache" should "yield an empty sequence of Pkgs" in {
    assert(fixture.pkgs.isEmpty)
  }

  it should behave like typical(fixture)
}
