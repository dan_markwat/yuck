package com.markwat.yuck.cache

import java.io.InputStream
import java.net.URI
import java.time.Instant

import com.markwat.yuck._
import org.apache.commons.io.input.NullInputStream
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

trait PkgCacheBehaviors {
  this: FlatSpec with Matchers with MockFactory =>

  object Pkgs {
    val randomSize: Long = (Math.random() * 10).toLong

    val abc = Pkg("abc", "1.0", PkgRemote(new URI("http://localhost/abc"), SourceConf(new URI("http://localhost/"))), YuckyData(Instant.now(), "", Nil, "tar"))
    val abcData = new PkgData {
      override def consume[T](op: (InputStream) => T): T = op(new NullInputStream(randomSize))

      override def pkg: Pkg = Pkgs.abc
    }
  }

  def typical(fixture: => PkgCache): Unit = {
    it should "get Pkgs it puts in" in {
      val cache = fixture
      cache.put(Pkgs.abc)
      cache.get(PkgTarget(Pkgs.abc)) should equal(Pkgs.abc)
    }

    it should "throw an exception when an invalid Pkg entry is requested" in {
      val cache = fixture

      an[Exception] should be thrownBy {
        cache.get(PkgTarget(Pkgs.abc))
      }
      an[Exception] should be thrownBy {
        cache.get(Pkgs.abc)
      }
    }

    it should "throw an exception when an invalid PkgData entry is requested"

    it should "get PkgData it puts in" in {
      val cache = fixture

      cache.put(Pkgs.abcData)

      cache.get(Pkgs.abc).consume({ is => is.available() }) should be(Pkgs.randomSize)
    }

    it should "remove Pkgs it puts in" in {
      val cache = fixture
      cache.put(Pkgs.abc)
      cache.remove(PkgTarget(Pkgs.abc), removeData = false) should be(true)

      cache.put(Pkgs.abc)
      cache.remove(PkgTarget(Pkgs.abc), removeData = true) should be(true)
    }

    it should "remove PkgData it puts in" in {
      val cache = fixture
      cache.put(Pkgs.abcData)
      cache.remove(Pkgs.abc, removeMetadata = false) should be(true)

      cache.put(Pkgs.abcData)
      cache.remove(Pkgs.abc, removeMetadata = true) should be(true)
    }

    it should "not throw exceptions on an invalid remove request" in {
      val cache = fixture
      cache.remove(PkgTarget(Pkgs.abc), removeData = false) should be(false)
      cache.remove(PkgTarget(Pkgs.abc), removeData = true) should be(false)

      cache.remove(Pkgs.abc, removeMetadata = false) should be(false)
      cache.remove(Pkgs.abc, removeMetadata = true) should be(false)
    }
  }
}
