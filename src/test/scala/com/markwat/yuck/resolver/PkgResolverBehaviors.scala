package com.markwat.yuck.resolver

import com.markwat.yuck.serde.Deser
import com.markwat.yuck.{Pkg, PkgData, PkgResource, PkgTarget}
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 6/21/2017.
  */
trait PkgResolverBehaviors {
  this: FlatSpec with Matchers =>

  trait Fixture {
    def target: PkgTarget

    def targetPkg: Pkg

    def targetData: PkgData

    def targets: Seq[PkgTarget]

    def discovered: Seq[PkgResource]

    def targetsData: Seq[PkgData]

    def reifier: Deser[Boolean]
  }

  def typical(resolver: => PkgResolver, fixture: Fixture): Unit = {

    it should "resolve all pkges for a given target" in {
      resolver.resolve(fixture.target) should equal(fixture.targetData)
    }

    it should "fetch the correct pkg data for a given pkg" in {
      resolver.resolve(fixture.targets) should equal(fixture.targetsData)
    }

    it should "discover the full graph available from the configuration" in {
      resolver.discover(fixture.targets).toSeq should equal(fixture.discovered)
    }

    it should "fetch the correct pkg data for a given pkg resource" in {
      val data = resolver.fetch(fixture.targetPkg)
      data.pkg should equal(fixture.targetData.pkg)
      data.consume(fixture.reifier) should equal(fixture.targetData.consume(fixture.reifier))
    }
  }
}
