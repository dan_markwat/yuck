package com.markwat.yuck.resolver

import java.io.InputStream
import java.time.Instant

import com.markwat.yuck._
import com.markwat.yuck.client.WebClient
import com.markwat.yuck.serde.Deser
import org.mockserver.client.server.MockServerClient
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 6/21/2017.
  */
class NPkgResolverSpec extends FlatSpec with Matchers with PkgResolverBehaviors with WebClient {

  override def expectations(mockServer: MockServerClient): Unit = {
  }

  def resolver: PkgResolver = ???

  "A pkg resolver with one source" should behave like typical(resolver, new Fixture {
    lazy val target: PkgTarget = targetPkg.target

    lazy val targetPkg: Pkg =
      Pkg(
        "abc",
        "1.0",
        PkgRemote("http://localhost:8090/abc/1.0/".uri, SourceConf("http://localhost:8090/".uri)),
        YuckyData(Instant.now(), "1", Seq(PkgTarget("xyz", "1.0")), "tar")
      )

    lazy val targetData: PkgData = new PkgData {
      override def pkg: Pkg = targetPkg

      override def consume[T](op: (InputStream) => T): T = ???
    }

    lazy val targets: Seq[PkgTarget] = ???

    lazy val discovered: Seq[PkgResource] = ???

    lazy val targetsData: Seq[PkgData] = ???

    override def reifier: Deser[Boolean] = ???
  })
}
