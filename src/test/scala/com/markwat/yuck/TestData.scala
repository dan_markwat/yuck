package com.markwat.yuck

import java.net.URI
import java.nio.charset.Charset
import java.time.Instant

import org.apache.commons.io.IOUtils

import scala.collection.JavaConverters._

/**
  * Created by me on 6/15/17.
  */
object TestData {
  val pkgAbc = Pkg("abc", "1.0",
    PkgRemote(new URI("http://localhost/abc/1.0/"),
      SourceConf(new URI("http://localhost/"))),
    YuckyData(Instant.parse("2017-05-23T15:48:44.533Z"), "1", PkgTarget("xyz", "1.0") :: Nil, "tar"))

  def load(file: String): String = {
    IOUtils.readLines(this.getClass.getResourceAsStream(file), Charset.forName("UTF-8"))
      .asScala.mkString("\n")
  }
}
