package com.markwat.yuck.client

import java.net.URI

import org.mockserver.client.server.MockServerClient
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.scalatest._

/**
  * Created by danie on 6/14/2017.
  */
class HttpSimpleClientSpec extends FlatSpec with Matchers with WebClient with SimpleClientBehaviors {

  override def expectations(mockServer: MockServerClient): Unit = {
    mockServer.when(
      request().withMethod("GET").withPath("/foo/bar") //, Times.exactly(1)
    ).respond(
      response().withStatusCode(200).withBody("")
    )

    mockServer.when(
      request().withMethod("HEAD").withPath("/foo/bar")
    ).respond(
      response().withStatusCode(200)
    )
  }

  "An http client" should behave like typical(new HttpSimpleClient, new Fixture {
    override def path: URI = host.resolve("/foo/bar")

    override def invalidPath: URI = host.resolve("/poo/too")

    override def invalidScheme: URI = new URI("file://abc/123")
  })
}
