package com.markwat.yuck.client

import java.net.URI
import java.nio.file.Paths

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 5/17/2017.
  */
class FileSimpleClientSpec extends FlatSpec with Matchers with SimpleClientBehaviors {

  val resolver = new FileSimpleClient

  val fixture = new Fixture {
    override def path: URI = Paths.get("src", "test", "resources", "testsite", "abc", "1.0", "metadata.json").toUri

    override def invalidPath: URI = Paths.get("does", "not", "exist").toUri

    override def invalidScheme: URI = new URI("http://localhost/")
  }

  "A local client" should behave like typical(resolver, fixture)
}
