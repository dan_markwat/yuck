package com.markwat.yuck.client

import java.net.URI
import java.util.UUID

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danie on 5/17/2017.
  */
trait SimpleClientBehaviors {
  this: FlatSpec with Matchers =>

  trait Fixture {
    def path: URI

    def invalidPath: URI

    def invalidScheme: URI
  }

  def typical(client: => SimpleClient, fixture: Fixture): Unit = {
    resource.managed(client) map { client =>
      it should "fail when the wrong scheme is used" in {
        noException should be thrownBy {
          client.exists(fixture.invalidPath)
        }

        an[Exception] should be thrownBy {
          client.exists(fixture.invalidScheme)
        }
      }

      it should "support the expected schema" in {
        client.supports(fixture.invalidScheme) should be(false)
      }

      it should "evaluate existence of a URI correctly" in {
        client.exists(fixture.path) should be(true)
      }

      it should "show non-existence of an invalid path" in {
        client.exists(fixture.invalidPath) should be(false)
      }

      it should "handle non-existence by throwing an exception" in {
        an[Exception] should be thrownBy {
          client.data(fixture.path.resolve(UUID.randomUUID().toString))({ is => is.close() })
        }
      }
    }
  }
}
