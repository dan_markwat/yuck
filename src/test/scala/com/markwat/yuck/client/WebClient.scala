package com.markwat.yuck.client

import java.net.URI

import org.mockserver.client.server.MockServerClient
import org.mockserver.integration.ClientAndServer
import org.scalatest.{BeforeAndAfterEach, Suite}

import scala.collection.mutable

/**
  * Created by me on 6/15/17.
  */
trait WebClient extends BeforeAndAfterEach {
  this: Suite =>

  val port: Int = WebClient.port

  var mockServer: MockServerClient = null

  def host: URI = new URI(s"http://localhost:$port/")

  def expectations(mockServer: MockServerClient)

  override def beforeEach(): Unit = {
    super.beforeEach()
    mockServer = ClientAndServer.startClientAndServer(this.port)

    // wires up all expectations for this web client
    this.expectations(mockServer)
  }

  override def afterEach(): Unit = {
    super.afterEach()
    mockServer.stop(true)
  }
}

object WebClient {

  private[this] val ports: mutable.Set[Int] = mutable.Set.empty

  def port: Int = synchronized {
    var p: Int = 0
    do {
      p = (Math.random() * 1000 + 8000).toInt
    } while (ports.contains(p))
    ports.add(p)
    p
  }
}
