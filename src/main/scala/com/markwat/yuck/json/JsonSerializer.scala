package com.markwat.yuck.json

import java.io.{InputStream, StringReader}
import java.nio.charset.Charset

import com.markwat.yuck.serde.{Deser, Ser}
import org.apache.commons.io.input.ReaderInputStream
import org.json4s.ext.JavaTypesSerializers
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization
import org.json4s.{DefaultFormats, Formats, StreamInput}

/**
  * Serializer and deserializer for Json data.
  */
object JsonSerializer {
  implicit val formats: Formats = DefaultFormats ++ JavaTypesSerializers.all + InstantSerializer

  def serializer[T <: AnyRef]: Ser[T] = (t: T) => new ReaderInputStream(new StringReader(Serialization.writePretty(t)), Charset.forName("UTF-8"))

  /**
    * Note: make sure to pass the type param in explicitly or [[Nothing]] may be resolved as the type to deserialize.
    *
    * @param mf
    * @tparam T
    * @return
    */
  def deserializer[T](implicit mf: scala.reflect.Manifest[T]): Deser[T] = (is: InputStream) => resource.managed(is).map({ is =>
    parse(StreamInput(is), useBigDecimalForDouble = true, useBigIntForLong = true).extract[T]
  }).tried.get
}
