package com.markwat.yuck.json

import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

import org.json4s.CustomSerializer
import org.json4s.JsonAST.JString

/**
  * Used to help with converting [[Instant]] data.
  */
class InstantSerializer(val format: DateTimeFormatter) extends CustomSerializer[Instant](_ => ( {
  case JString(s) => format.parse(s, (temporal: TemporalAccessor) => Instant.from(temporal))
}, {
  case t: Instant => JString(format.format(t))
}
))

object InstantSerializer extends InstantSerializer(DateTimeFormatter.ISO_INSTANT)
