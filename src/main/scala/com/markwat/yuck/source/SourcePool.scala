package com.markwat.yuck.source

import java.net.URI

import com.markwat.yuck.{Pkg, PkgData, PkgTarget}

/**
  * Wraps many [[RemoteSource]]s into a single pool.  Each [[RemoteSource]] can have its own configuration
  * and complex internals, so this was created to keep their usage tidy and scalable.
  */
trait SourcePool {

  /**
    * The wrapped [[RemoteSource]]s
    *
    * @return the wrapped [[RemoteSource]]s
    */
  def sources: Set[RemoteSource]

  /**
    * Obtains a [[RemoteSource]] capable of serving the given [[URI]].  There is no requirement that there only be
    * one [[RemoteSource]] to serve the [[URI]] - however, only one will be selected.
    *
    * @param serves the [[URI]] to obtain a [[RemoteSource]] to serve
    * @return a [[RemoteSource]] capable of serving the [[URI]]
    */
  def source(serves: URI): RemoteSource

  /**
    * Delegates to [[RemoteSource]]s - see [[SourcePool]] impl notes for info on how
    *
    * @param target the [[PkgTarget]] to test
    * @return true if one or many [[RemoteSource]]s host this target
    */
  def exists(target: PkgTarget): Boolean

  /**
    * Delegates to [[RemoteSource]]'s - see [[SourcePool]] impl notes for info on how
    *
    * @param target the [[PkgTarget]] to obtain [[Pkg]] metadata for
    * @return [[Pkg]] metadata if one or many [[RemoteSource]]s host this target
    */
  def metadata(target: PkgTarget): Pkg

  /**
    * Delegates to [[RemoteSource]]'s - see [[SourcePool]] impl notes for info on how
    *
    * @param pkg the [[Pkg]] to obtain [[PkgData]] for
    * @return [[PkgData]] if one or many [[RemoteSource]]s host this [[PkgData]]
    */
  def data(pkg: Pkg): PkgData
}
