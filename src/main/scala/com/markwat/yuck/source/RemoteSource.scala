package com.markwat.yuck.source

import java.net.URI

import com.markwat.yuck._

/**
  * Represents a source of [[Pkg]] and [[PkgData]].  "Remote" is used to mean anywhere that's not on the classpath.
  * [[RemoteSource]] implementations are entirely [[URI]]-based for the greatest level of flexibility.  This could
  * allow for us to use services like S3, HDFS, etc.
  */
trait RemoteSource {

  /**
    * The source's configuration.  This includes HTTP header information, auth information, etc.
    *
    * @return the [[SourceConf]] for this [[RemoteSource]]
    */
  def conf: SourceConf

  /**
    * Tests if this [[RemoteSource]] serves the given [[URI]].
    *
    * @param uri
    * @return
    */
  def serves(uri: URI): Boolean = {
    val source = conf.location
    val (scheme, host, port, path) = (source.getScheme, source.getHost, source.getPort, source.getPath)
    uri.getScheme == scheme && uri.getHost == host && uri.getPort == port && uri.getPath.startsWith(path)
  }

  /**
    * Tests if the given target is hosted out of this [[RemoteSource]].
    *
    * @param target the [[PkgTarget]] to test
    * @return true if the [[PkgTarget]] is hosted by this [[RemoteSource]], otherwise false
    */
  def exists(target: PkgTarget): Boolean

  /**
    * Obtains the [[Pkg]] metadata for the given [[PkgTarget]].  The [[RemoteSource]] makes its own
    * determination on which [[Pkg]] matches best.  Look to other methods for obtaining multiple
    * matches (future work).
    *
    * @param target the target to retrieve
    * @return the [[Pkg]] metadata associated with the [[PkgTarget]]
    */
  def metadata(target: PkgTarget): Pkg

  /**
    * Get the [[PkgData]] associated with the [[Pkg]].  This obtains the archive of files the [[Pkg]] refers to.
    * Implementations will vary on how [[PkgData]]'s [[java.io.InputStream]] is obtained and used.  Look
    * to the implementation notes to understand how the InputStream is created.
    *
    * @param pkg the [[Pkg]] to obtain for
    * @return the [[PkgData]] associated
    */
  def data(pkg: Pkg): PkgData
}
