package com.markwat.yuck.source

import java.io.InputStream

import com.markwat.yuck._
import com.markwat.yuck.client.SimpleClient
import com.markwat.yuck.layout.{DefaultRepositoryLayout, RepositoryLayout}
import com.markwat.yuck.serde.Deser
import com.typesafe.scalalogging.LazyLogging

/**
  * An implementation of [[RemoteSource]] wrapping a single simple client.
  */
class SingleRemoteSource(val conf: SourceConf)(implicit deser: Deser[Pkg]) extends RemoteSource with LazyLogging {

  lazy val client: SimpleClient = SimpleClient(conf)

  lazy val format: RepositoryLayout = new DefaultRepositoryLayout(conf.location)

  override def exists(target: PkgTarget): Boolean = {
    val path = format.metadata(target)
    val resolved = conf.location.resolve(path)

    client.exists(resolved)
  }

  override def metadata(target: PkgTarget): Pkg = {
    val metadataPath = format.metadata(target)

    logger.info(s"Attempting to resolve pkg metadata at ${metadataPath.toString}")

    client.data(metadataPath)(deser)
  }

  override def data(opkg: Pkg): PkgData = {
    val dataPath = format.data(opkg)

    new PkgData {
      override def pkg: Pkg = opkg

      override def consume[T](op: (InputStream) => T): T = client.data(dataPath)(op)
    }
  }

  override def toString = s"SingleRemoteSource($conf)"
}
