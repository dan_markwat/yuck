package com.markwat.yuck.source

import com.markwat.yuck._
import com.markwat.yuck.cache.PkgCache

/**
  * A [[RemoteSource]] impl wrapping a [[PkgCache]].  This is used to short-circuit requests out to external systems.
  * [[Pkg]] metadata is itself not designed to change once published, thus allowing for lengthy cache periods and
  * sparing yuck from requesting it again.
  *
  * Each method delegates to the [[PkgCache]].
  */
class CacheSource(pkgCache: PkgCache) extends RemoteSource {

  override val conf: SourceConf = SourceConf(pkgCache.rootPath.toUri)

  override def exists(target: PkgTarget): Boolean = pkgCache.has(target)

  override def metadata(target: PkgTarget): Pkg = pkgCache.get(target)

  override def data(pkg: Pkg): PkgData = pkgCache.get(pkg)
}
