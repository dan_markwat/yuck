package com.markwat.yuck.source

import java.net.URI

import com.markwat.yuck.{Pkg, PkgData, PkgTarget}
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

/**
  * A [[SourcePool]] that iterates over each [[RemoteSource]] in [[sources]], returning the first expected
  * value for each.
  */
class RemoteSourcePool(val sources: Set[RemoteSource]) extends SourcePool with LazyLogging {
  private[this] val poolMap = sources.groupBy(_.conf)

  logger.info(s"Using pool: ${poolMap.mkString(",")}")

  override def exists(target: PkgTarget): Boolean = {
    sources.toStream.exists(_.exists(target))
  }

  override def metadata(target: PkgTarget): Pkg = {
    sources.toStream.map(rs => Try(rs.metadata(target)).toEither).filter({
      case Left(throwable) =>
        logger.warn(s"Failed to resolve metadata from source", throwable)
        false
      case Right(_) => true
    }).head.right.get
  }

  override def data(pkg: Pkg): PkgData = poolMap(pkg.remote.sourceConf).toStream.map(_.data(pkg)).head

  override def source(serves: URI): RemoteSource = sources.toStream.filter(_.serves(serves)).head
}
