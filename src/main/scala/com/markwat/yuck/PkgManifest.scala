package com.markwat.yuck

import java.time.Instant

/**
  * A manifest for a [[Pkg]], including original metadata, timestamp of last operation (install),
  * and the files' path on the filesystem.
  *
  * @param target
  * @param timestamp
  * @param files
  */
case class PkgManifest(target: Pkg, timestamp: Instant, files: Seq[String])
