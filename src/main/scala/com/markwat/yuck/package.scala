package com.markwat

import java.net.URI

/**
  * Created by danie on 6/21/2017.
  */
package object yuck {

  private[yuck] class FromString(str: String) {
    def uri: URI = new URI(str)
  }

  implicit def fromString(str: String): FromString = new FromString(str)

  private[yuck] class FromPkg(pkg: Pkg) {
    def target: PkgTarget = PkgTarget(pkg)

    def resource: PkgResource = PkgResource(pkg)
  }

  implicit def fromPkg(pkg: Pkg): FromPkg = new FromPkg(pkg)

  private[yuck] class FromPkgResource(resource: PkgResource) {
    def target: PkgTarget = PkgTarget(resource)
  }

  implicit def fromPkgResource(resource: PkgResource): FromPkgResource = new FromPkgResource(resource)

}
