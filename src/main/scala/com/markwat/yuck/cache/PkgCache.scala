package com.markwat.yuck.cache

import java.io._
import java.nio.charset.Charset
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{FileVisitOption, Files, Path, Paths}

import com.markwat.yuck._
import com.markwat.yuck.layout.{DefaultRepositoryLayout, RepositoryLayout}
import com.markwat.yuck.serde.{Deser, Ser}
import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.io.IOUtils

import scala.collection.JavaConverters._
import scala.util.Try

/**
  * A map-like caching system that uses a configured local directory for managing [[Pkg]]-related entries.
  */
// todo add packaging configuration for pkg cache
class PkgCache(cacheDir: Path)(implicit ser: Ser[Pkg], deser: Deser[Pkg]) extends Object with LazyLogging {

  private[yuck] def rootPath: Path = cacheDir

  val repoFormat: RepositoryLayout = RepositoryLayout(cacheDir.toUri)

  def resolvedPkgDir(pkg: PkgTarget): Path = {
    val dir = Paths.get(repoFormat.path(pkg))
    dir.toFile.mkdirs()
    dir
  }

  def has(pkg: PkgTarget): Boolean = new File(repoFormat.metadata(pkg)).exists()

  def has(pkg: Pkg): Boolean = new File(repoFormat.data(pkg)).exists()

  private def getPkgFile(pkg: PkgTarget): File = {
    resolvedPkgDir(pkg).resolve("metadata.json").toFile
  }

  private def getPkgDataFile(pkg: Pkg): File = {
    resolvedPkgDir(pkg).resolve(s"""${pkg.fqn}-${pkg.version}.${pkg.yuckyData.packaging}""").toFile
  }

  def get(target: PkgTarget): Pkg = {
    deser(new FileInputStream(getPkgFile(target)))
  }

  def get(pkg: Pkg): PkgData = {
    val pkgDataFile = this.getPkgDataFile(pkg)

    if(!pkgDataFile.exists())
      throw new FileNotFoundException(pkgDataFile.getAbsolutePath + " does not exist")

    CachedPkgData(pkg, () => new FileInputStream(pkgDataFile))
  }

  // metadata collected for each pkg
  def pkgs: Seq[Pkg] = {
    // for closing the stream
    resource.managed(Files.find(
      rootPath,
      2,
      (path: Path, attrs: BasicFileAttributes) => path.toFile.getName == "metadata.json" && attrs.isRegularFile,
      FileVisitOption.FOLLOW_LINKS)).map({ metadataFiles =>

      metadataFiles.iterator().asScala.map(path =>
        resource.managed(new FileInputStream(path.toFile)).map(deser(_)).tried.get).toVector
    }).tried.get
  }

  def put(pkg: Pkg): Pkg = {
    val file = this.getPkgFile(pkg)

    resource.managed(ser(pkg)).map({
      pkgStream =>
        resource.managed(new PrintWriter(file)).map({
          pw => IOUtils.copy(pkgStream, pw, Charset.forName("UTF-8"))
        }).tried.get
    }).tried.get

    pkg
  }

  def put(pkgData: PkgData): PkgData = {
    val file = getPkgDataFile(pkgData.pkg)
    val proc = (is: InputStream) => {
      resource.managed(new FileOutputStream(file)).map({ fos =>
        IOUtils.copy(is, fos)
      }).tried.get
    }
    pkgData.consume(proc)
    CachedPkgData(pkgData.pkg, () => new FileInputStream(file))
  }

  def remove(target: PkgTarget, removeData: Boolean): Boolean = {
    val metadataRemoved = () => Try(get(target)).toOption.map(this.remove(_, removeMetadata = false))
    if (removeData) getPkgFile(target).delete() || metadataRemoved().nonEmpty
    else getPkgFile(target).delete()
  }

  def remove(pkg: Pkg, removeMetadata: Boolean): Boolean = {
    logger.info(s"Removing ${getPkgDataFile(pkg)}")
    val removed = getPkgDataFile(pkg).delete()
    if (removeMetadata) removed || this.remove(PkgTarget(pkg), removeData = false)
    else removed
  }
}
