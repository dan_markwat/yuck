package com.markwat.yuck.cache

import java.io.InputStream

import com.markwat.yuck.{Pkg, PkgData, PkgTarget}

import scala.util.Try

/**
  * An intermediary for working indirectly with a [[PkgCache]].  This facilitates a get-or-put operation
  * which [[PkgCache]] does not handle.
  */
trait CacheController {

  /**
    * Optionally get the [[Pkg]] associated with a given [[PkgTarget]].
    *
    * @param pkgTarget the [[PkgTarget]] to obtain a [[Pkg]] for
    * @return [[Option]] of [[Pkg]]
    */
  def get(pkgTarget: PkgTarget): Option[Pkg]

  /**
    * Optionally get the [[PkgData]] associated with a given [[Pkg]].
    *
    * @param pkg the [[Pkg]] to obtain a [[PkgData]] for
    * @return [[Option]] of [[PkgData]]
    */
  def get(pkg: Pkg): Option[PkgData]

  /**
    * Get a cached [[Pkg]] object given a [[PkgTarget]] or put the given [[Pkg]] in the cache if it doesn't exist.
    *
    * @param target the [[PkgTarget]] to use
    * @param pkg    the [[Pkg]] to associate with [[PkgTarget]] if a cached entry isn't found
    * @return the cached [[Pkg]] data
    */
  def cached(target: PkgTarget)(pkg: => Pkg): Pkg

  /**
    * Get a cached [[PkgData]] object given a [[Pkg]] or put the given [[PkgData]] in the cache if it doesn't exist.
    *
    * @param pkg     the [[Pkg]] to use
    * @param pkgData the [[PkgData]] to associate with the [[Pkg]] if a cached entry isn't found
    * @return the cached [[PkgData]]
    */
  def cached(pkg: Pkg)(pkgData: => PkgData): PkgData
}

object CacheController {

  /**
    * A [[CacheController]] that directly and synchronously communicates with a [[PkgCache]].
    *
    * @param pkgCache the [[PkgCache]] backing this controller
    */
  class DirectSyncCacheController(pkgCache: PkgCache) extends CacheController {

    override def get(pkgTarget: PkgTarget): Option[Pkg] = Try(pkgCache.get(pkgTarget)).toOption

    override def get(pkg: Pkg): Option[PkgData] = Try(pkgCache.get(pkg)).toOption

    override def cached(target: PkgTarget)(pkgGen: => Pkg): Pkg = {
      get(target).getOrElse {
        val pkg = pkgGen
        pkgCache.put(pkg)
        pkg
      }
    }

    // todo need a way more elegant way of doing this
    override def cached(pkg: Pkg)(pkgDataGen: => PkgData): PkgData = {
      val externPkg = pkg

      new PkgData {
        override def pkg: Pkg = externPkg

        override def consume[T](op: (InputStream) => T): T = Try(get(pkg).map(_.consume(op)).get).toOption.getOrElse {
          val pkgData = pkgDataGen
          pkgCache.put(pkgData)
          pkgData.consume(op)
        }
      }
    }
  }

}
