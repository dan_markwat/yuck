package com.markwat.yuck.cache

import java.io.InputStream

import com.markwat.yuck.{Pkg, PkgData}

/**
  * Created by danie on 6/14/2017.
  */
case class CachedPkgData(pkg: Pkg, input: () => InputStream) extends PkgData {

  /**
    * Consume the [[InputStream]] using `op`
    *
    * @param op
    * @tparam T
    * @return
    */
  override def consume[T](op: (InputStream) => T): T = {
    op(input())
  }
}
