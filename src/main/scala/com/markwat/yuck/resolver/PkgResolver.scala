package com.markwat.yuck.resolver

import com.markwat.yuck.graph.PkgGraph
import com.markwat.yuck.resolver.PkgResolver.DependencyGraph
import com.markwat.yuck.{Pkg, PkgData, PkgResource, PkgTarget}

/**
  * The core component for resolving [[Pkg]]'s and associated [[PkgData]].
  *
  * Responsible for assessing the requested [[PkgTarget]]'s and resolving the
  * version(s) requested.
  */
// todo prepare for and implement version ranges concept
// todo when ranges implemented: add all matching versions to graph (?)
trait PkgResolver {

  def graphFactory: () => PkgGraph

  /**
    * Resolves a single target based on the version target provided.
    *
    * @param target
    * @return
    */
  def resolve(target: PkgTarget): Pkg

  def resolve(targets: Seq[PkgTarget]): Seq[Pkg] = {
    targets.map(this.resolve)
  }

  /**
    * Discovers all dependencies in the graph given a starting point.
    *
    * @param target
    * @return
    */
  def discover(target: PkgTarget, targets: PkgTarget*): PkgGraph = {
    this.discover(targets :+ target)
  }

  def discover(targets: Seq[PkgTarget]): PkgGraph = {
    val pkg = this.resolve(targets)
    val explored = DependencyGraph(pkg).complete(p => p.yuckyData.dependencies.map(this.resolve)).explored
    val graph = graphFactory()
    explored.foreach(graph.add)
    graph
  }

  /**
    * Fetches a single package's data.
    *
    * @param pkg
    * @return
    */
  def fetch(pkg: Pkg): PkgData

  /**
    * Fetches a single [[PkgData]] based on the minimal [[PkgResource]] information.
    *
    * @param pkgResource the [[PkgResource]] to obtain [[PkgData]] for
    * @return the associated [[PkgData]]
    */
  def fetch(pkgResource: PkgResource): PkgData
}

object PkgResolver {

  /**
    * Resolves a full, raw dependency graph.  Duplicate named packages can exist in this class,
    * so each dependency should be processed after full resolution to select the appropriate
    * pkgs and versions.
    */
  case class DependencyGraph(frontier: Seq[Pkg], explored: Set[Pkg]) {

    def isComplete: Boolean = frontier.isEmpty

    // iterates once on the 'frontier', returns a new graph
    def explore(fn: Pkg => Seq[Pkg]): DependencyGraph = {
      frontier.foldLeft(this.copy(frontier = Nil))({
        case (graph@DependencyGraph(nFrontier, nExplored), pkg: Pkg) =>
          if (!nExplored.contains(pkg)) {
            val frontierCandidates = fn.apply(pkg)
            val nextFrontier = frontierCandidates.toSet -- nExplored
            graph.copy(frontier = nFrontier ++ nextFrontier, explored = nExplored + pkg)
          } else
            graph
      })
    }

    // completes the exploration
    def complete(fn: Pkg => Seq[Pkg]): DependencyGraph = {
      var graph = this
      while (!graph.isComplete) {
        graph = graph.explore(fn)
      }
      graph
    }
  }

  object DependencyGraph {

    def apply(start: Pkg): DependencyGraph = DependencyGraph(Seq(start), Set())

    def apply(start: Seq[Pkg]): DependencyGraph = DependencyGraph(start, Set())
  }

}
