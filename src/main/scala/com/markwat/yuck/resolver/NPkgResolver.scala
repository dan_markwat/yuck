package com.markwat.yuck.resolver

import com.markwat.yuck.cache.CacheController
import com.markwat.yuck.graph.PkgGraph
import com.markwat.yuck.source.SourcePool
import com.markwat.yuck.{Pkg, PkgData, PkgResource, PkgTarget}

/**
  * Implementation of a [[PkgResolver]] that uses a [[CacheController]] to manage how [[Pkg]]-related
  * information is accessed.
  */
class NPkgResolver(source: SourcePool, val graphFactory: () => PkgGraph, cache: CacheController) extends PkgResolver {

  override def resolve(target: PkgTarget): Pkg = cache.cached(target) {
    source.metadata(target)
  }

  override def fetch(pkg: Pkg): PkgData = cache.cached(pkg) {
    source.data(pkg)
  }

  override def fetch(pkgResource: PkgResource): PkgData = {
    val r = this.resolve(PkgTarget(pkgResource.fqn, pkgResource.version))
    this.fetch(r)
  }
}
