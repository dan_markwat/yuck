package com.markwat.yuck.action

import com.markwat.yuck.action.Action.Options

/**
  * An action defined for yuck.
  */
trait Action[T <: Options] {

  def run(options: T): Unit
}

object Action {

  /**
    * Root options trait
    */
  trait Options {
  }

}
