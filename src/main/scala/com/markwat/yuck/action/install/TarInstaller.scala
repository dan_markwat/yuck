package com.markwat.yuck.action.install

import java.io.FileOutputStream
import java.nio.file.Path

import com.markwat.yuck.action.Installer
import com.markwat.yuck.install.{InstallContext, InstalledContext}
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.io.IOUtils

/**
  * Installs from tar archives.
  */
object TarInstaller extends Installer {

  override def apply(ic: InstallContext): InstalledContext = {
    val pkg = ic.pkgData
    val root = ic.root
    pkg.consume({ is =>
      val files = Set.newBuilder[Path]

      resource.managed(new TarArchiveInputStream(is)).map({ tar =>

        // todo make sure we handle fifos, devices, sparse files, etc. correctly
        while (tar.getNextTarEntry != null) {
          // get current entry
          val entry = tar.getCurrentEntry

          // resolve rooted path & file
          val entryPath = root.resolve("./" + entry.getLinkName)
          val entryFile = entryPath.toFile

          // add file to set
          files += entryPath.toAbsolutePath

          // todo map users, groups, etc. (before or after?)

          resource.managed(new FileOutputStream(entryFile)).map({ fos =>
            val arr = Array.ofDim[Byte](entry.getSize.toInt)
            val bytesRead = IOUtils.read(tar, arr)
            val bytesWritten = IOUtils.write(arr, fos)

            // todo add assertion about bytes read & bytes read vs written
          }).tried.get
        }
      }).tried.get

      // ascending order paths by name
      val sorted = files.result().toSeq.sortBy(_.toString)(Ordering.String)

      InstalledContext(root, sorted)
    })
  }
}
