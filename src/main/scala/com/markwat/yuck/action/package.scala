package com.markwat.yuck

import com.markwat.yuck.action.install.TarInstaller
import com.markwat.yuck.install.{InstallContext, InstalledContext}

/**
  * Package-level types and classes for the [[action]] package.
  */
package object action {

  /**
    * Responsible for performing extraction and "installation" of a [[Pkg]].
    */
  type Installer = InstallContext => InstalledContext

  object Installer {

    def apply(packaging: String): Installer = packaging match {
      case "tar" => TarInstaller
      case "tar.gz" => ???
      case _ => ???
    }
  }

}
