package com.markwat.yuck.action

import com.markwat.yuck._
import com.markwat.yuck.action.Install.InstallOptions
import com.markwat.yuck.action.install.TarInstaller
import com.markwat.yuck.reactor.Request.InstallReq
import com.markwat.yuck.reactor.{InstallReactor, Plan}
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Try}

/**
  * The 'install' action.  Responsible for installing [[Pkg]]s based on [[PkgTarget]]s or a [[Plan]], and can
  * be used to create and save [[Plan]]s for other yuck systems to use.
  */
class Install(ctx: YuckContext) extends Action[InstallOptions] {

  def install(targets: Seq[PkgTarget]): Unit = {
    new InstallReactor(ctx.resolver, ctx.inventory)
  }

  def save(targets: Seq[PkgTarget]): Unit = {
    val reactor = new InstallReactor(ctx.resolver, ctx.inventory)

    val plan = reactor.plan(targets.map(InstallReq))

    println(plan)
  }

  override def run(options: InstallOptions): Unit = {
    options.action match {
      case "install" => this.install(options.targets)
      case "save" => this.save(options.targets)
    }
  }
}

object Install extends Object with LazyLogging {

  def apply(): Install = {
    val ctx = YuckContext(YuckConfig())
    new Install(ctx)
  }

  def apply(ctx: YuckContext): Install = new Install(ctx)

  case class InstallOptions(action: String = "", targets: Seq[PkgTarget] = Nil) extends Action.Options

  def main(args: Array[String]): Unit = Try {
    val parser = new scopt.OptionParser[InstallOptions]("yuck-clean") {
      head("yuck-install", "1.0-beta")

      opt[String]('a', "action").required().action((a, opts) => opts.copy(action = a))
        .text("the action to perform - one of [install, save]")

      arg[String]("targets").required().unbounded().action((target, opts) =>
        opts.copy(targets = opts.targets :+ {
          val x = target.split('@')
          logger.debug(s"Pkg target arg: ${x.mkString(" @ ")}")
          PkgTarget(x(0), x(1))
        })).text("pkg targets to operate on")

      checkConfig(opts =>
        if (opts.action.isEmpty) failure("need to specify an action")
        else success)
    }

    val install = Install()

    parser.parse(args, InstallOptions()) match {
      case Some(opts) => install.run(opts)
      case None => System.exit(1)
    }
  } recoverWith {
    case throwable =>
      logger.error("Install error", throwable)
      System.exit(1)
      Failure(throwable)
  }

}
