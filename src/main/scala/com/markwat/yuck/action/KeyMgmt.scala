package com.markwat.yuck.action

import scala.sys.process._

/**
  * Add, remove, list gpg keys in use by Yuck based on a [[com.markwat.yuck.YuckConfig]].
  */
class KeyMgmt {
  // list
  // remove
  // add

  def list() = {
    ("gpg --fingerprint").!!.split("""""")
  }

  def remove() = {}
}

case class YuckyPGPKey(uid: String, user: String, email: String, fingerprint: String, pub: String, sub: String)
