package com.markwat.yuck.action

import java.io._
import java.nio.file.Paths

import com.markwat.yuck._
import com.markwat.yuck.action.Action.Options
import com.markwat.yuck.action.Resolve.ResolveOptions
import com.markwat.yuck.graph.{DeducedPkgGraph, Level}
import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.io.IOUtils
import resource.managed

import scala.util.{Failure, Try}

/**
  * The 'resolve' action.  Responsible for resolving [[Pkg]]s and printing their dependency trees
  * or downloading their [[PkgData]].
  */
class Resolve(ctx: YuckContext) extends Action[ResolveOptions] with LazyLogging {

  /**
    * Print the resolved dependencies.
    *
    * @return
    */
  def print(opts: ResolveOptions, pkgs: DeducedPkgGraph): Unit = {
    val template = (p: PkgResource) => s"${p.fqn} @ ${p.version} (${p.location})"

    if (opts.treeOutput) {
      def recursePrint(level: Level): Unit = {
        val pad = "    " * level.depth
        level.level.foreach { pkg =>
          println(pad + template(pkg))
          recursePrint(level.at(pkg))
        }
      }

      recursePrint(pkgs.tree)
    } else
      pkgs.ordered.foreach { p =>
        println(template(p))
      }
  }

  /**
    * Download all the dependencies.
    *
    * @return
    */
  def download(opts: ResolveOptions, pkgs: DeducedPkgGraph): Unit = {
    val path = opts.downloadDir.toPath
    pkgs.ordered.foreach { pkgResource =>
      val data = ctx.resolver.fetch(pkgResource)
      val file = path.resolve(s"${pkgResource.fqn}-${pkgResource.version}.tar.gz").toFile

      data.consume(dataStream =>
        managed(new FileOutputStream(file)).map({ fos =>
          IOUtils.copy(dataStream, fos)
        }).tried.get
      )
    }
  }

  override def run(ropts: ResolveOptions): Unit = {
    val action = ropts.action
    val targets = ropts.targets

    logger.info(s"Using targets: ${targets.mkString(", ")}")

    // retrieve/discover dependency metadata
    val graph = ctx.resolver.discover(targets)

    // deduce & report cycles
    if (graph.hasCycles)
      throw new IllegalStateException("Cycles detected in dependency graph - can't proceed \n" + graph.cycles().mkString("\n"))

    // deduce specific pkg versions & resolve remaining conflicts
    val deduced = graph.deduce()

    // perform final actions
    action match {
      case "print" => this.print(ropts, deduced)
      case "download" => this.download(ropts, deduced)
    }
  }
}

object Resolve extends LazyLogging {

  def apply(): Resolve = {
    val ctx = YuckContext(YuckConfig())
    new Resolve(ctx)
  }

  def apply(ctx: YuckContext): Resolve = new Resolve(ctx)

  case class ResolveOptions(action: String = "",
                            targets: Seq[PkgTarget] = Nil,
                            saveLoc: String = Paths.get("save.ttl").toString,
                            treeOutput: Boolean = false,
                            downloadDir: File = Paths.get("").toFile) extends Options

  def main(args: Array[String]): Unit = Try {
    val parser = new scopt.OptionParser[ResolveOptions]("yuck-resolve") {
      head("yuck-resolve", "1.0-beta")

      opt[String]('a', "action").required().action((a, opts) => opts.copy(action = a))
        .text("the action to perform - one of [print, download, save]")

      opt[String]('s', "save-loc").optional().maxOccurs(1).action((s, opts) => opts.copy(saveLoc = s))
        .text("the location to save the pkg graph (save action only)")

      opt[File]("download-dir").optional().maxOccurs(1).action((f, opts) => opts.copy(downloadDir = f))
        .validate(f => if (f.isDirectory) success else failure("must be a directory"))
        .text("the location to save downloaded files")

      opt[Unit]("tree-output").action((_, opts) => opts.copy(treeOutput = true))
        .text("whether the output should be printed in tree format (default false)")

      arg[String]("targets").required().unbounded().action((target, opts) =>
        opts.copy(targets = opts.targets :+ {
          val x = target.split('@')
          logger.debug(s"Pkg target arg: ${x.mkString(" @ ")}")
          PkgTarget(x(0), x(1))
        })).text("pkg targets to operate on")

      checkConfig(opts =>
        if (opts.action.isEmpty) failure("action must be specified")
        else success)
    }

    val resolve = Resolve()

    parser.parse(args, ResolveOptions()) match {
      case Some(opts) => resolve.run(opts)
      case None => System.exit(1)
    }
  } recoverWith {
    case throwable =>
      logger.error("Resolve error", throwable)
      System.exit(1)
      Failure(throwable)
  }
}
