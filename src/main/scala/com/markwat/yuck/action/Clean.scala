package com.markwat.yuck.action

import com.markwat.yuck.action.Clean.CleanOptions
import com.markwat.yuck.{YuckConfig, YuckContext}
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Try}

/**
  * The 'clean' action.  Responsible for cleaning up cached entries, etc.
  */
class Clean(ctx: YuckContext) extends Action[CleanOptions] with LazyLogging {

  def pkgs(): Unit = {
    val cache = ctx.pkgCache
    cache.pkgs.foreach { pkg =>
      logger.info(s"Removing ${pkg.fqn}@${pkg.version} metadata: ${
        if (cache.remove(pkg, removeMetadata = false)) "SUCCESS" else "FAILED"
      }")
    }
  }

  def all(): Unit = {
    val cache = ctx.pkgCache
    cache.pkgs.foreach { pkg =>
      logger.info(s"Removing all ${pkg.fqn}@${pkg.version} data: ${
        if (cache.remove(pkg, removeMetadata = true)) "SUCCESS" else "FAILED"
      }")
    }
  }

  override def run(copts: CleanOptions): Unit = {
    val action = copts.action

    action match {
      case "pkgs" => this.pkgs()
      case "all" => this.all()
    }
  }
}

object Clean extends Object with LazyLogging {

  def apply(): Clean = {
    val ctx = YuckContext(YuckConfig())
    new Clean(ctx)
  }

  def apply(ctx: YuckContext): Clean = new Clean(ctx)

  case class CleanOptions(action: String = "") extends Action.Options

  def main(args: Array[String]): Unit = Try {
    val parser = new scopt.OptionParser[CleanOptions]("yuck-clean") {
      head("yuck-clean", "1.0-beta")

      opt[String]('a', "action").required().action((a, opts) => opts.copy(action = a))
        .text("the action to perform - one of [pkgs, all]")

      checkConfig(opts =>
        if (opts.action.isEmpty) failure("need to specify an action")
        else success)
    }

    val clean = Clean()

    parser.parse(args, CleanOptions()) match {
      case Some(opts) => clean.run(opts)
      case None => System.exit(1)
    }
  } recoverWith {
    case throwable =>
      logger.error("Clean error", throwable)
      System.exit(1)
      Failure(throwable)
  }
}
