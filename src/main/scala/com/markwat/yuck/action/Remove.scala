package com.markwat.yuck.action

import com.markwat.yuck.action.Remove.RemoveOptions
import com.markwat.yuck.reactor.InstallReactor
import com.markwat.yuck.reactor.Request.RemoveReq
import com.markwat.yuck.{PkgTarget, YuckConfig, YuckContext}
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Try}

/**
  * The 'remove' action.  Responsible for removing installed [[com.markwat.yuck.Pkg]]s from the system.
  */
class Remove(ctx: YuckContext) extends Action[RemoveOptions] with LazyLogging {

  def remove(targets: Seq[PkgTarget]): Unit = {
    val reactor = new InstallReactor(ctx.resolver, ctx.inventory)
    val plan = reactor.plan(targets.map(RemoveReq))
    reactor.process(plan)
  }

  override def run(options: RemoveOptions): Unit = {
    this.remove(options.targets)
  }
}

object Remove extends Object with LazyLogging {

  def apply(): Remove = {
    val ctx = YuckContext(YuckConfig())
    new Remove(ctx)
  }

  def apply(ctx: YuckContext): Remove = new Remove(ctx)

  case class RemoveOptions(targets: Seq[PkgTarget] = Nil) extends Action.Options

  def main(args: Array[String]): Unit = Try {
    val parser = new scopt.OptionParser[RemoveOptions]("yuck-remove") {
      head("yuck-remove", "1.0-beta")

      arg[String]("targets").required().unbounded().action((target, opts) =>
        opts.copy(targets = opts.targets :+ {
          val x = target.split('@')
          logger.debug(s"Pkg target arg: ${x.mkString(" @ ")}")
          PkgTarget(x(0), x(1))
        })).text("pkg targets to operate on")

      checkConfig(opts =>
        if (opts.targets.isEmpty) failure("need to specify targets")
        else success)
    }

    val remove = Remove()

    parser.parse(args, RemoveOptions()) match {
      case Some(opts) => remove.run(opts)
      case None => System.exit(1)
    }
  } recoverWith {
    case throwable =>
      logger.error("Remove error", throwable)
      System.exit(1)
      Failure(throwable)
  }

}
