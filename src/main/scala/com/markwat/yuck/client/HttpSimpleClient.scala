package com.markwat.yuck.client

import java.io.InputStream
import java.net.URI

import com.typesafe.scalalogging.LazyLogging
import fs2.{Strategy, Task}
import org.http4s.Status.{ClientError, ServerError, Successful}
import org.http4s.client.blaze.PooledHttp1Client
import org.http4s.{Method, Request, Uri}

/**
  * Simple HTTP client based on http4s.
  */
class HttpSimpleClient extends SimpleClient with LazyLogging {

  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global
  implicit val startegy = Strategy.fromExecutionContext(ec)

  // todo took away bc it needs to be shutdown or the JVM hangs
  //implicit val scheduler = Scheduler.fromScheduledExecutorService(new ScheduledThreadPoolExecutor(5))

  private[this] val client = PooledHttp1Client()

  override def exists(path: URI): Boolean = {
    if (!this.supports(path)) throw new UnsupportedOperationException(s"scheme,${path.getScheme}, not supported")

    client.fetch(Request(Method.HEAD, Uri.fromString(path.toString).right.get))({
      case Successful(r) if 200.until(300).contains(r.status.code) => Task.now(true)
      case r => Task.now(false)
    })
      //.unsafeTimed(FiniteDuration(20000, TimeUnit.MILLISECONDS))
      .unsafeRun()
  }

  override def supportedSchemes: Set[String] = Set("http", "https")

  override def data[T](path: URI)(op: (InputStream) => T): T = {
    logger.info(path.toString)
    client.get(Uri.fromString(path.toString).right.get)({
      case Successful(resp) =>
        logger.debug(s"success (${resp.status}) when querying $path")
        fs2.io.toInputStream[Task].apply(resp.body).map(op).runLog
      case ClientError(resp) =>
        logger.warn(s"client error (${resp.status}) when querying $path")
        fs2.io.toInputStream[Task].apply(throw new Exception(s"client error (${resp.status}) when querying $path")).map(op).runLog
      case ServerError(resp) =>
        logger.warn(s"server error (${resp.status}) when querying $path")
        fs2.io.toInputStream[Task].apply(throw new Exception(s"server error (${resp.status}) when querying $path")).map(op).runLog
    }).unsafeRun().head
  }

  override def close(): Unit = client.shutdownNow()
}
