package com.markwat.yuck.client

import java.io.{File, FileInputStream, InputStream}
import java.net.URI

/**
  * Simple file system client.
  */
class FileSimpleClient extends SimpleClient {

  override def exists(path: URI): Boolean = new File(path).exists()

  override def supportedSchemes: Set[String] = Set("file", null)

  override def data[T](path: URI)(op: (InputStream) => T): T = op(new FileInputStream(new File(path)))

  override def close(): Unit = ()
}
