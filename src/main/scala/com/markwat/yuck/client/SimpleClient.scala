package com.markwat.yuck.client

import java.io.InputStream
import java.net.URI

import com.markwat.yuck.SourceConf

/**
  * A simple wrapper around various client for [[com.markwat.yuck.source.RemoteSource]] implementations to use.
  */
trait SimpleClient extends AutoCloseable {

  def supportedSchemes: Set[String]

  def supports(path: URI): Boolean = supportedSchemes.contains(path.getScheme)

  def exists(path: URI): Boolean

  def data[T](path: URI)(op: InputStream => T): T
}

object SimpleClient {

  def apply(sourceConf: SourceConf): SimpleClient = sourceConf.location.getScheme match {
    case "http" | "https" => new HttpSimpleClient
    case "file" | null => new FileSimpleClient
    case x => throw new NotImplementedError(s"""no resolver designed for "$x""")
  }
}
