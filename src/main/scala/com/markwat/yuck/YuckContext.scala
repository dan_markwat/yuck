package com.markwat.yuck

import com.markwat.yuck.Inventory.InventoryImpl
import com.markwat.yuck.cache.CacheController.DirectSyncCacheController
import com.markwat.yuck.cache.{CacheController, PkgCache}
import com.markwat.yuck.graph.{JenaPkgGraph, PkgGraph}
import com.markwat.yuck.json.JsonSerializer
import com.markwat.yuck.resolver.{NPkgResolver, PkgResolver}
import com.markwat.yuck.serde.{Deser, Ser}
import com.markwat.yuck.source.{RemoteSource, RemoteSourcePool, SingleRemoteSource, SourcePool}
import com.softwaremill.macwire._

/**
  * Used as a rooted context based on an immutable configuration.  Provides a concrete source for
  * any wried dependencies.
  *
  */
trait YuckContext {

  /**
    * @return the wired configuration
    */
  def config: YuckConfig

  /**
    * @return the [[CacheController]] to use
    */
  def cacheController: CacheController

  /**
    * @return the [[PkgResolver]] to use
    */
  def resolver: PkgResolver

  /**
    * @return the [[Set]] of [[RemoteSource]] taken from the configured sources
    */
  def remoteSources: Set[RemoteSource]

  /**
    * @return the [[SourcePool]] the [[PkgResolver]] will use
    */
  def sourcePool: SourcePool

  /**
    * @return the concrete [[PkgCache]] implementation
    */
  def pkgCache: PkgCache

  /**
    * @return the graph factory we will use to construct new [[PkgGraph]]s
    */
  def graphFactory: () => PkgGraph

  /**
    * @return the configured [[Inventory]] to use
    */
  def inventory: Inventory
}

object YuckContext {

  def apply(): YuckContext = apply(YuckConfig())

  def apply(config: YuckConfig): YuckContext = new YuckContextImpl(config)

  class YuckContextImpl(val config: YuckConfig) extends YuckContext {

    implicit val pkgSerializer: Ser[Pkg] = JsonSerializer.serializer
    implicit val pkgDeserializer: Deser[Pkg] = JsonSerializer.deserializer
    implicit val manifestSer: Ser[PkgManifest] = JsonSerializer.serializer
    implicit val manifestDeser: Deser[PkgManifest] = JsonSerializer.deserializer

    lazy val cacheController: CacheController = wire[DirectSyncCacheController]

    lazy val resolver: PkgResolver = wire[NPkgResolver]

    lazy val remoteSources: Set[RemoteSource] = config.sources.map(new SingleRemoteSource(_))

    lazy val sourcePool: SourcePool = wire[RemoteSourcePool]

    lazy val pkgCache: PkgCache = new PkgCache(config.cacheDir)

    lazy val graphFactory: () => PkgGraph = () => JenaPkgGraph.memory()

    lazy val inventory: Inventory = new InventoryImpl(config.inventoryDir)
  }

}
