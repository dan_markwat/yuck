package com.markwat.yuck.graph

import com.markwat.yuck.PkgResource
import com.markwat.yuck.graph.Level.Distance

/**
  * Represents levels of a dependency DAG.  This is implemented as branched levels
  * with a single root level.  Each level can have multiple entries and only depends on
  * the previous fqn of a pkg.
  */
trait Level {
  /**
    * The distrance from this level to the root level
    *
    * @return distance to the root level
    */
  def depth: Distance

  /**
    * Get the next [[Level]] corresponding with the [[PkgResource]].
    *
    * @param pkgResource the [[PkgResource]] for the next [[Level]]
    * @return the next [[Level]] or an empty [[Level]]
    */
  def at(pkgResource: PkgResource): Level

  /**
    *
    * @return the entries in this [[Level]]
    */
  def level: Set[PkgResource]

  override def toString: String = s"$depth@[${level.mkString("[", ",", "]")}: ${level.map(this.at(_).toString)}]"
}

object Level {
  type Distance = Int

  def apply(): RootLevel = RootLevel(Map.empty)

  def apply(leveled: Set[(PkgResource, PkgResource, Distance)]): RootLevel = {
    val levels = leveled
      .groupBy({
        case (prev, next, distance) => (prev, distance)
      })
      .mapValues(_.map(_._2))
      .withDefaultValue(Set.empty)

    RootLevel(levels)
  }

  case class RootLevel(levels: Map[(PkgResource, Distance), Set[PkgResource]]) extends Level {
    val depth: Distance = 0

    def at(pkgResource: PkgResource): LevelAt = {
      LevelAt(levels((pkgResource, depth)), depth + 1, levels)
    }

    def level: Set[PkgResource] = levels.keys.filter(_._2 == depth).map(_._1).toSet
  }

  case class LevelAt(level: Set[PkgResource], depth: Distance, levels: Map[(PkgResource, Distance), Set[PkgResource]]) extends Level {
    def at(pkgResource: PkgResource): LevelAt = {
      if (level contains pkgResource)
        LevelAt(levels((pkgResource, depth)), depth + 1, levels)
      else
        LevelAt(Set.empty, depth + 1, levels)
    }
  }

}
