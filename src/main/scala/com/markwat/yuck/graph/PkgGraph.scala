package com.markwat.yuck.graph

import com.markwat.yuck.{Pkg, PkgResource}

/**
  * A [[Pkg]] dependency graph.
  */
trait PkgGraph extends Iterable[PkgResource] with AutoCloseable {

  /**
    * Adds a [[Pkg]] to the graph.
    *
    * @param pkg the [[Pkg]] to add
    * @return the [[PkgResource]] added
    */
  // todo see if returning the PkgResource is obsolete
  def add(pkg: Pkg): PkgResource

  /**
    * Tests if this [[PkgGraph]] has any cyclic dependencies.
    *
    * @return true if a cycle exists, false otherwise
    */
  def hasCycles: Boolean

  /**
    * Tells if all dependencies are satisfied.
    *
    * @return true if all dependencies are satisfied
    */
  def isComplete: Boolean

  /**
    * Gets all the cycles that exist in the graph.  An empty [[Seq]] will be returned
    * if no cycles exist.
    *
    * @return the [[Seq]] of cycles - each entry is a [[Set]] of [[PkgResource]]s involved in that cycle
    */
  def cycles(): Seq[Set[PkgResource]]

  /**
    * Derive a graph capable of performing operations pertinent to [[Pkg]] resolution.
    *
    * @return a [[DeducedPkgGraph]]
    */
  def deduce(): DeducedPkgGraph
}

/**
  * A derived [[PkgGraph]] capable of performing resolution-related operations.
  */
trait DeducedPkgGraph {

  /**
    *
    * @return a topologically-sorted [[Seq]] of [[PkgResource]]s
    */
  def ordered: Seq[PkgResource]

  /**
    *
    * @return a tree-like structure where each level is an upstream dependency of its parent entry in a preceding level
    */
  def tree: Level
}
