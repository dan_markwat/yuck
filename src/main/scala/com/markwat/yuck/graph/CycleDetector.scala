package com.markwat.yuck.graph

import com.markwat.yuck.graph.CycleDetector.Vertex

import scala.collection.mutable

/**
  * Dijkstra's strongly connected components implementation.  This was planned for use in [[PkgGraph]]
  * implementations to find any cycles if they exist.
  *
  * This was used as the reference implementation: https://gist.github.com/gyaikhom/8e85d19408998b365b08
  *
  * @param vertices
  */
class CycleDetector(vertices: mutable.Map[String, Vertex]) {
  var pStack: List[String] = Nil
  var sStack: List[String] = Nil

  var preorder = 1
  var scc = 1

  val components: mutable.Map[Int, Seq[Vertex]] = mutable.Map.empty.withDefaultValue(Seq())

  def dfs(id: String): Unit = {
    vertices.update(id, vertices(id).copy(preorder = preorder))
    preorder = preorder + 1

    sStack = id :: sStack
    pStack = id :: pStack

    for {
      adjId <- vertices(id).adj
    } {
      if (vertices(adjId).preorder == -1)
        dfs(adjId)
      else if (vertices(adjId).scc == -1)
        while (vertices(pStack.head).preorder > vertices(adjId).preorder)
          pStack = pStack.tail
    }

    if (pStack.head == id) {
      var scc_found = 0
      var wOpt: Option[String] = {
        val opt = sStack.headOption
        sStack = sStack.tail
        opt
      }
      while (wOpt.nonEmpty) {
        val w = wOpt.get
        if (vertices(w).scc == -1) {
          if (scc_found == 0) {
            // Console.print(s"scc $scc: ")
            scc_found = 1
          }
          vertices.update(w, vertices(w).copy(scc = scc))
          // Console.print(s"$w ")
          components.update(scc, components(scc) :+ vertices(w))
        }
        if (id == w) {
          if (scc_found != 0) {
            // Console.println()
            scc = scc + 1
          }
          // break
          wOpt = None
        }
        if (wOpt.nonEmpty)
          wOpt = {
            val opt = sStack.headOption
            sStack = sStack.tail
            opt
          }
      }
      pStack = pStack.tail
    }
  }

  def run(): Unit = {
    vertices.keys.foreach(v => dfs(v))
  }
}

object CycleDetector {

  case class Vertex(id: String, adj: Seq[String], preorder: Int = -1, scc: Int = -1) {
    def deg: Int = adj.size
  }

}
