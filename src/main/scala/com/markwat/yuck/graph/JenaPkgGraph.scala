package com.markwat.yuck.graph

import java.io.File
import java.nio.file.Files

import com.markwat.yuck.graph.JenaPkgGraph.{JenaDeducedPkgGraph, ReadTxn}
import com.markwat.yuck.graph.JenaPkgResource.Types
import com.markwat.yuck.{Pkg, PkgResource}
import com.typesafe.scalalogging.LazyLogging
import org.apache.jena.ontology.{OntModel, OntModelSpec}
import org.apache.jena.query._
import org.apache.jena.rdf.model.{Model, ModelFactory}
import org.apache.jena.tdb.TDBFactory
import org.apache.jena.util.FileManager
import resource._

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.util.Try

/**
  * This implementation of [[PkgGraph]] is based on Apache Jena, a system and framework for creating and
  * querying RDF graphs.  [[Pkg]] metadata is mapped into an OWL-based ontology model so it can be queried
  * any number of ways according to the SPARQL and OWL specs defined by W3C.
  */
class JenaPkgGraph(dataset: Dataset) extends PkgGraph with LazyLogging {
  // to help with extracting the URI of a Resource

  private lazy val ontology: Model = {
    val owlFile = getClass.getResource("/yuck.owl").toURI
    FileManager.get().loadModel(new File(owlFile).getAbsolutePath, "RDF/XML")
  }

  private lazy val baseModel: OntModel = {
    ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, dataset.getDefaultModel.add(ontology))
  }

  private lazy val types: Types = new Types(baseModel)

  override def add(pkg: Pkg): PkgResource = JenaPkgGraph.WriteTxn(dataset) {
    // Pkg individual & properties
    val pkgInd = types.pkgClass.createIndividual(pkg.remote.location.toString)
    pkgInd
      .addLiteral(types.published_on, pkg.yuckyData.published.toEpochMilli)
      .addLiteral(types.has_version, pkg.version)
      .addLiteral(types.has_fqn, pkg.fqn)

    // corresponding PkgTarget & properties
    val pkgTargetInd = types.pkgTargetClass.createIndividual()
    // add property for version & fqn
    pkgTargetInd
      .addLiteral(types.has_fqn, pkg.fqn)
      .addLiteral(types.has_version, pkg.version)

    pkg.yuckyData.dependencies.foreach { next =>
      // will need to make sure that name & version are separate and added as triples
      val dependencyInd = types.pkgTargetClass.createIndividual()
      dependencyInd
        .addLiteral(types.has_fqn, next.fqn)
        .addLiteral(types.has_version, next.version)

      pkgInd.addProperty(types.depends_on, dependencyInd)
    }

    dataset.commit()

    PkgResource(pkg)
  }

  override def hasCycles: Boolean = JenaPkgGraph.ReadTxn(dataset) {
    val detectCyclesQuery = QueryFactory.create(
      s"""
         |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
         |PREFIX owl: <http://www.w3.org/2002/07/owl#>
         |PREFIX : <https://www.github.com/dmarkwat/ontologies/yuck#>
         |ASK
         |{
         |  ?pkgUri rdf:type :Pkg ;
         |          (:dependsOn / :hasFQN / ^:hasFQN)+ ?d .
         |
         |  ?d rdf:type :Pkg .
         |
         |  FILTER(?d = ?pkgUri)
         |}
       """.stripMargin)

    managed(QueryExecutionFactory.create(detectCyclesQuery, dataset))
      .map(_.execAsk())
      .tried.get
  }

  // todo holy poo this is complicated - needs help
  override def cycles(): Seq[Set[PkgResource]] = ReadTxn(dataset) {
    val pkgUri = "pkgUri"
    val detectCyclesQuery = QueryFactory.create(
      s"""
         |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
         |PREFIX owl: <http://www.w3.org/2002/07/owl#>
         |PREFIX : <https://www.github.com/dmarkwat/ontologies/yuck#>
         |SELECT ?$pkgUri
         |WHERE
         |{
         |  ?$pkgUri rdf:type :Pkg ;
         |          (:dependsOn / :hasFQN / ^:hasFQN)+ ?d .
         |
         |  ?d rdf:type :Pkg .
         |
         |  FILTER(?d = ?$pkgUri)
         |}
       """.stripMargin)

    val sansCycles = QueryFactory.create(
      s"""
         |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
         |PREFIX owl: <http://www.w3.org/2002/07/owl#>
         |PREFIX : <https://www.github.com/dmarkwat/ontologies/yuck#>
         |SELECT ?$pkgUri
         |WHERE
         |{
         |  ?$pkgUri rdf:type :Pkg .
         |  MINUS {
         |    ?$pkgUri rdf:type :Pkg ;
         |            (:dependsOn / :hasFQN / ^:hasFQN)+ ?d .
         |
         |    ?d rdf:type :Pkg .
         |
         |    FILTER(?d = ?$pkgUri)
         |  }
         |}
      """.stripMargin)

    val allCycles = managed(QueryExecutionFactory.create(detectCyclesQuery, dataset))
      .map(_.execSelect().asScala
        .map(rs => {
          val resource = rs.getResource(pkgUri)
          JenaPkgResource(resource, types)
        }).toVector)
      .tried.get

    val remainder = managed(QueryExecutionFactory.create(detectCyclesQuery, dataset))
      .map(_.execSelect().asScala
        .map(rs => {
          val resource = rs.getResource(pkgUri)
          JenaPkgResource(resource, types)
        }).toVector)
      .tried.get

    val cycleSets = Set.empty ++ allCycles :: Set.empty ++ remainder :: Nil
    cycleSets.filter(_.nonEmpty)
  }

  /**
    * Deduce the best set of packages to install, in order.  Fail if solution can't be found.
    */
  override def deduce(): DeducedPkgGraph = {
    if (isComplete)
      new JenaDeducedPkgGraph(dataset, types, baseModel)
    else
      throw new IllegalStateException("Graph must be complete to deduce")
  }

  override def iterator: Iterator[PkgResource] = ReadTxn(dataset) {
    val pkgUri = "pkgUri"
    val selectAll = QueryFactory.create(
      s"""
         |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
         |PREFIX owl: <http://www.w3.org/2002/07/owl#>
         |PREFIX : <https://www.github.com/dmarkwat/ontologies/yuck#>
         |SELECT ?$pkgUri
         |WHERE
         |{
         |  ?$pkgUri rdf:type :Pkg .
         |}
      """.stripMargin)

    managed(QueryExecutionFactory.create(selectAll, dataset)).map(exec =>
      exec.execSelect().asScala.map(rs => {
        val resource = rs.getResource(pkgUri)
        JenaPkgResource(resource, types)
      }).toVector).tried.get.toIterator
  }

  /**
    * Tells if all dependencies are satisfied.
    *
    * @return
    */
  override def isComplete: Boolean = ReadTxn(dataset) {
    val askHasUnsatisfiedDep = QueryFactory.create(
      s"""
         |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
         |PREFIX owl: <http://www.w3.org/2002/07/owl#>
         |PREFIX : <https://www.github.com/dmarkwat/ontologies/yuck#>
         |ASK
         |{
         |  ?target a :PkgTarget ;
         |          ^:dependsOn ?x ;
         |          minus {
         |            ?target :hasFQN / ^:hasFQN ?p .
         |            ?p a :Pkg .
         |          }
         |}
       """.stripMargin)

    // if the query yields true, it means there is a PkgTarget without a Pkg defined for it and the graph is therefore incomplete
    !managed(QueryExecutionFactory.create(askHasUnsatisfiedDep, dataset))
      .map(_.execAsk())
      .tried.get
  }

  override def close(): Unit = dataset.close()
}

object JenaPkgGraph {

  def tdb(): JenaPkgGraph = {
    val temp = Files.createTempDirectory("jena")
    temp.toFile.deleteOnExit()
    new JenaPkgGraph(TDBFactory.createDataset(temp.toString))
  }

  def memory(): JenaPkgGraph = {
    new JenaPkgGraph(TDBFactory.createDataset())
  }

  object ReadTxn {
    def apply[O](dataset: Dataset)(fn: => O): O = {
      dataset.begin(ReadWrite.READ)
      val r = Try {
        fn
      }
      dataset.end()
      r.get
    }
  }

  object WriteTxn {
    def apply[O](dataset: Dataset)(fn: => O): O = {
      dataset.begin(ReadWrite.WRITE)
      val r = Try {
        fn
      }
      dataset.end()
      r.get
    }
  }

  class JenaDeducedPkgGraph(dataset: Dataset, types: Types, model: Model) extends DeducedPkgGraph {

    override def ordered: Seq[PkgResource] = ReadTxn(dataset) {
      val pkgUri = "pkgUri"
      val nextPkg = "nextPkg"
      val distance = "distance"

      /*
       * Some tips taken from https://stackoverflow.com/questions/5198889/calculate-length-of-path-between-nodes
       *
       * pkgUri is the root pkg (is not depended on)
       * nextPkg is the dependency pkg
       * distance is the distance from the root pkg
       *
       * sorted so that the pkgs furthest down the dependency tree are returned first,
       * thus allowing for installation in order without error
       */
      val selectAll = QueryFactory.create(
        s"""
           |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
           |PREFIX owl: <http://www.w3.org/2002/07/owl#>
           |PREFIX : <https://www.github.com/dmarkwat/ontologies/yuck#>
           |SELECT ?$pkgUri ?$nextPkg (count(?mid) - 1 as ?$distance)
           |WHERE
           |{
           |  ?$pkgUri rdf:type :Pkg ;
           |          (:dependsOn / :hasFQN / ^:hasFQN)* ?mid ;
           |          minus {
           |            ?$pkgUri (:hasFQN / ^:hasFQN / ^:dependsOn)+ ?upstream .
           |            ?upstream a :Pkg .
           |          } .
           |
           |  ?mid rdf:type :Pkg ;
           |            (:dependsOn / :hasFQN / ^:hasFQN)* ?$nextPkg .
           |  ?$nextPkg a :Pkg .
           |}
           |group by ?$pkgUri ?$nextPkg
           |order by desc(?$distance)
      """.stripMargin)

      // using linked hash set for guaranteed ordering by insertion
      val linkedHashSet = mutable.LinkedHashSet.empty ++ managed(QueryExecutionFactory.create(selectAll, dataset))
        .map(exec =>
          exec.execSelect()
            .asScala
            .map(rs => {
              //              val resource = rs.getResource(pkgUri)
              val next = rs.getResource(nextPkg)
              //              val dist = rs.getLiteral(distance).getInt
              //              println(s"$resource:$next:$dist")
              JenaPkgResource(next, types)
            }).toVector).tried.get

      linkedHashSet.toSeq
    }

    override def tree: Level = ReadTxn(dataset) {
      // defines the starting pkgs (no upstream deps)
      val startPkg = "startPkg"
      // the target for purposes of measuring length
      val targetPkg = "targetPkg"
      // the next pkg beyond the target
      val nextPkg = "nextPkg"
      // the distance from the target to its start
      val distance = "distance"

      /*
       * Similar to the ordered() method's sparql query
       */
      val selectAll = QueryFactory.create(
        s"""
           |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
           |PREFIX owl: <http://www.w3.org/2002/07/owl#>
           |PREFIX : <https://www.github.com/dmarkwat/ontologies/yuck#>
           |SELECT ?$startPkg ?$targetPkg ?$nextPkg ?$distance
           |WHERE
           |{
           |  ?$targetPkg :dependsOn / :hasFQN / ^:hasFQN ?$nextPkg .
           |  ?$nextPkg a :Pkg .
           |  {
           |    SELECT ?$startPkg ?$targetPkg (count(?mid) - 1 as ?$distance)
           |    WHERE
           |    {
           |      ?$startPkg rdf:type :Pkg ;
           |              (:dependsOn / :hasFQN / ^:hasFQN)* ?mid ;
           |              minus {
           |                ?$startPkg (:hasFQN / ^:hasFQN / ^:dependsOn)+ ?upstream .
           |                ?upstream a :Pkg .
           |              } .
           |
           |      ?mid rdf:type :Pkg ;
           |            (:dependsOn / :hasFQN / ^:hasFQN)* ?$targetPkg .
           |      ?$targetPkg a :Pkg .
           |    }
           |    group by ?$startPkg ?$targetPkg
           |  }
           |}
      """.stripMargin)

      val set = managed(QueryExecutionFactory.create(selectAll, dataset))
        .map(exec =>
          exec.execSelect()
            .asScala
            .map(rs => {
              // only needed in query for purposes of grouping
              //              val start = rs.getResource(startPkg)
              val target = rs.getResource(targetPkg)
              val next = rs.getResource(nextPkg)
              val dist = rs.getLiteral(distance).getInt

              //              println(s"$start:$target:$next:$dist")

              (JenaPkgResource(target, types), JenaPkgResource(next, types), dist)
            }).toVector).tried.get.toSet

      Level(set)
    }
  }

}
