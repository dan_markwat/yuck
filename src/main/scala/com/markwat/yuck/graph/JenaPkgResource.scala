package com.markwat.yuck.graph

import java.net.URI

import com.markwat.yuck.PkgResource
import org.apache.jena.ontology.{DatatypeProperty, ObjectProperty, OntClass, OntModel}
import org.apache.jena.rdf.model.Resource

/**
  * Assists in mapping Apache Jena [[Resource]]s to [[PkgResource]]s.
  */
object JenaPkgResource {

  def apply(resource: Resource, types: Types): PkgResource = {
    PkgResource(
      fqn = resource.getProperty(types.has_fqn).getString,
      version = resource.getProperty(types.has_version).getString,
      location = new URI(resource.getURI)
    )
  }

  val rootURI = "https://www.github.com/dmarkwat/ontologies/yuck#"

  val PKG = rootURI + "Pkg"
  val PKG_TARGET = rootURI + "PkgTarget"

  val DEPENDS_ON = rootURI + "dependsOn"
  val PUBLISHED_ON = rootURI + "publishedOn"
  val HAS_VERSION = rootURI + "hasVersion"
  val HAS_FQN = rootURI + "hasFQN"

  /**
    * Contains Ontology classes and properties constructed from an [[OntModel]].  This class only gets
    * the classes from the model - they should already be present in the ontology based on an RDF/XML file.
    * References are obtained and stored here and this class is passed around to spare Jena the trouble
    * of obtaining the references repeatedly.
    *
    * @param model the [[OntModel]] to obtain the classes and properties from.
    */
  class Types(model: OntModel) {
    // classes
    lazy val pkgClass: OntClass = model.getOntClass(PKG)
    lazy val pkgTargetClass: OntClass = model.getOntClass(PKG_TARGET)

    // object properties
    lazy val depends_on: ObjectProperty = model.getObjectProperty(DEPENDS_ON)

    // datatype properties
    lazy val published_on: DatatypeProperty = model.getDatatypeProperty(PUBLISHED_ON)
    lazy val has_version: DatatypeProperty = model.getDatatypeProperty(HAS_VERSION)
    lazy val has_fqn: DatatypeProperty = model.getDatatypeProperty(HAS_FQN)
  }

}
