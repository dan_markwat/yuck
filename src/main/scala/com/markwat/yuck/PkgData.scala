package com.markwat.yuck

import java.io.InputStream

/**
  * Wrapper around pkg data.  Various formats or caching could be supported
  * and this trait exposes the ways to interact with it.
  *
  */
// todo needs more concrete implementations - many exist as anonymous impls
trait PkgData {

  def pkg: Pkg

  // todo needs to change - may eventually be > 1 packaging to choose from
  def packaging: String = pkg.yuckyData.packaging

  /**
    * Consume the [[InputStream]] using `op`
    *
    * @param op
    * @tparam T
    * @return
    */
  def consume[T](op: InputStream => T): T
}
