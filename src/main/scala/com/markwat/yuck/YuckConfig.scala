package com.markwat.yuck

import java.net.URI
import java.nio.file.{Path, Paths}

import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.LazyLogging

import scala.collection.JavaConverters._

/**
  * The configuration for Yuck.
  */
trait YuckConfig {

  /**
    * @return the raw configuration data
    */
  def confData: Config

  /**
    * @return the site directory where all yuck operations are rooted
    */
  def siteDir: Path

  /**
    * @return the directory of the configured inventory
    */
  def inventoryDir: Path

  /**
    * @return the directory where all cache data is stored
    */
  def cacheDir: Path

  /**
    * @return the configured sources
    */
  def sources: Set[SourceConf]
}

object YuckConfig extends LazyLogging {

  def apply(): YuckConfig = {
    apply(ConfigFactory.load())
  }

  def apply(config: Config): YuckConfig = {
    logger.debug(s"Loaded config: ${config.toString}")
    val yuckConf = new YuckConfigImpl(config)

    // todo verify config data - need to catch this as soon as possible

    yuckConf
  }

  class YuckConfigImpl(val confData: Config) extends YuckConfig {
    lazy val rootConf: Config = confData.getConfig("yuck")

    lazy val siteDir: Path = Paths.get(rootConf.getString("site"))

    lazy val inventoryDir: Path = Paths.get(rootConf.getString("inventory"))

    lazy val cacheDir: Path = Paths.get(rootConf.getString("cache"))

    lazy val sources: Set[SourceConf] = rootConf.getStringList("sources")
      .asScala.map(s => SourceConf(new URI(s)))
      .toSet
  }

}
