package com.markwat.yuck

import java.net.URI

/**
  *
  * @param location the location of the 'root' of the pkg (not a specific file)
  * @param sourceConf the source used to obtain metadata and data for the [[Pkg]] this corresponds with
  */
case class PkgRemote(location: URI, sourceConf: SourceConf)
