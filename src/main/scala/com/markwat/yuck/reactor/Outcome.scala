package com.markwat.yuck.reactor

import com.markwat.yuck.reactor.Fulfillment.{Fulfilled, Unfulfilled}

/**
  * Derived types define the result of [[Plan]] execution.
  */
trait Outcome {

  /**
    *
    * @return the [[Plan]] this [[Outcome]] originated from
    */
  def plan: Plan
}

object Outcome {

  /**
    * The [[Plan]] execution was completed successfully.
    *
    * @param plan    the source [[Plan]]
    * @param results the [[Seq]] of [[Fulfilled]] [[Step]]s
    */
  case class Complete(plan: Plan, results: Seq[Fulfilled]) extends Outcome

  /**
    * The [[Plan]] was not completed.
    *
    * @param plan    the source [[Plan]]
    * @param results the [[Seq]] of [[Either]] [[Unfulfilled]] or [[Fulfilled]] [[Step]]s
    */
  case class Incomplete(plan: Plan, results: Seq[Either[Unfulfilled, Fulfilled]]) extends Outcome

}
