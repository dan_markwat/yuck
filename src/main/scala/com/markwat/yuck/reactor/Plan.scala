package com.markwat.yuck.reactor

import com.markwat.yuck.reactor.Fulfillment.{Fulfilled, Unfulfilled}

/**
  * A plan for installation/upgrade/removal/etc. of [[com.markwat.yuck.Pkg]]'s.
  *
  * Each [[Plan]] is intended to be known up-front, allowing us to output it to a file.
  * These plans can then be read-in and operated on without the need for [[com.markwat.yuck.Pkg]]
  * discovery.
  *
  * Operating from a Plan will be assumed to be brute-force once it's created
  * and accepted for implementation.  The only exceptions thrown will be due to hard issues
  *
  */
case class Plan(steps: Seq[Step])
