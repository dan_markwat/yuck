package com.markwat.yuck.reactor

/**
  * Encapsulates the actions necessary to negotiate installation/upgrade/removal of [[com.markwat.yuck.Pkg]]'s
  * from [[Request]] inputs.
  */
trait Reactor {

  /**
    * Constructs a [[Plan]] from the given requests.
    *
    * @param requests
    * @return
    */
  def plan(requests: Seq[Request]): Plan
}

object Reactor {

  /**
    * Thrown when 2 [[com.markwat.yuck.Pkg]]es are depended on, or one is installed, and
    * the requirements on a particular version of that Pkg can't be met.
    *
    * E.g. A@1.0 depends on B@2.0 but B@1.0 is installed.  Further, the dependencies that led to
    * B@1.0 being installed do not allow for B@2.0 to be installed in B@1.0's place.
    *
    * Another example of collision would be if A@1.0 depends on B@2.0 but another Pkg requested be
    * installed, C@2.1, depends on B@2.3 but does not allow for B@2.0.
    */
  case class CollisionDetected() extends Exception

}
