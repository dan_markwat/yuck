package com.markwat.yuck.reactor

import com.markwat.yuck.PkgResource

/**
  * A step in a [[Plan]].
  */
trait Step

object Step {

  case class InstallStep(resource: PkgResource) extends Step

}
