package com.markwat.yuck.reactor

/**
  * Represents the status of a Step operation.
  */
trait Fulfillment {
  def isSuccess: Boolean
}

object Fulfillment {

  trait Fulfilled {
    this: Fulfillment =>

    override def isSuccess: Boolean = true
  }

  trait Unfulfilled {
    this: Fulfillment =>

    override def isSuccess: Boolean = false
  }

  case class Installed() extends Fulfillment with Fulfilled

  case class Removed() extends Fulfillment with Fulfilled

  case class Rejected() extends Fulfillment with Unfulfilled

  case class Failed() extends Fulfillment with Unfulfilled

  case class NotRun() extends Fulfillment with Unfulfilled
}
