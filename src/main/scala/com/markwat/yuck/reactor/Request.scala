package com.markwat.yuck.reactor

import com.markwat.yuck.{PkgResource, PkgTarget}

/**
  * A request for a yuck reactor to perform some action.
  */
trait Request

object Request {

  case class InstallReq(target: PkgTarget) extends Request

  case class UpdateReq(target: PkgTarget, predecing: PkgResource) extends Request

  case class RemoveReq(resource: PkgTarget) extends Request

  case class VersionConflict() extends Exception

  case class UnresolvedDependency(message: String) extends Exception(message)

}
