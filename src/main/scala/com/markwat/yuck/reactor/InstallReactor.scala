package com.markwat.yuck.reactor

import com.markwat.yuck.Inventory.Result
import com.markwat.yuck._
import com.markwat.yuck.action.Installer
import com.markwat.yuck.reactor.Fulfillment._
import com.markwat.yuck.reactor.Outcome.{Complete, Incomplete}
import com.markwat.yuck.reactor.Request.{InstallReq, UnresolvedDependency, VersionConflict}
import com.markwat.yuck.reactor.Step.InstallStep
import com.markwat.yuck.resolver._

/**
  * Installation Reactor.
  */
// todo add cleanup of resolver?  where?
class InstallReactor(resolver: PkgResolver, inventory: Inventory) extends Reactor {

  /**
    * Construct a plan for the input install requests.
    * [[InstallReq]] transitions:
    * to [[reactor.Request.UpdateReq]]: when a [[PkgTarget]] is already installed at the wrong version but can be upgraded according to [[com.markwat.yuck.YuckyData]].
    *
    * @throws VersionConflict      when an Update can't be performed on a pkg due to [[YuckyData]] restrictions
    * @throws UnresolvedDependency when a [[Pkg]] can't be resolved from a [[PkgTarget]]
    * @param targets
    * @return
    */
  override def plan(targets: Seq[Request]): Plan = {
    // construct dependency graph
    // note: resolver makes decision on "best fit" for a given PkgTarget
    // reduce to PkgGraph
    val graph = resolver.discover(targets.map { case InstallReq(target) => target })

    // evaluate cycles & completeness
    if (graph.hasCycles)
      throw new IllegalStateException("Dependency graph has cycles")

    // transform PkgGraph into an ordered sequence of Requests/Steps
    if (!graph.isComplete)
      throw UnresolvedDependency("Dependency graph isn't complete")

    // perform any transitions
    // todo needs more work to ensure we capture any update steps (?)
    val steps = graph.deduce().ordered.map(pkgR => InstallStep(pkgR))

    // return as Plan
    Plan(steps)
  }

  // todo add collision detection as a verification step before processing the Plan (requires all installed pkgs be tested)
  // could use jena: add all package-file mappings and query for collisions
  def process(plan: Plan): Outcome = {
    /**
      * Failing state: continue processing as though an upstream step has not succeeded.
      *
      * @param stream
      * @param results
      * @return
      */
    def failing(stream: Stream[Step], results: List[Either[Unfulfilled, Fulfilled]]): Outcome = {
      Incomplete(plan, stream.map(_ => Left(NotRun())) ++: results)
    }

    /**
      * Succeeding state: continue processing as though all steps prior succeeded.
      *
      * @param stream
      * @param results
      * @return
      */
    def succeeding(stream: Stream[Step], results: List[Fulfilled]): Outcome = {
      if (stream.isEmpty)
        Complete(plan, results)
      else {
        val result = stream.head match {
          // todo add logic for picking installer to use (delegate to an Installer which decides based on packaging?)
          case InstallStep(resource) =>
            val pkgData = resolver.fetch(resource)
            // get installer based on packaging
            val installer = Installer(pkgData.pkg.yuckyData.packaging)
            inventory.install(pkgData)(installer) match {
              case Result.Failed => Failed()
              case Result.Succeeded => Installed()
            }
        }
        result match {
          case s: Fulfilled => succeeding(stream.tail, s +: results)
          case f: Unfulfilled => failing(stream.tail, Left(f) +: results.map(Right(_)))
        }
      }
    }

    succeeding(plan.steps.toStream, Nil)
  }
}
