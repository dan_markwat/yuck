package com.markwat.yuck

import java.io.InputStream

/**
  * Basic serializer and deserializer types to accommodate various classes' needs for serializing and reifying
  * [[Pkg]] objects.
  */
package object serde {

  /**
    * Type to serialize any object
    *
    * @tparam T
    */
  type Ser[T] = T => InputStream

  /**
    * Type to deserialize any object
    *
    * @tparam T
    */
  type Deser[T] = InputStream => T

}
