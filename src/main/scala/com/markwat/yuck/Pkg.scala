package com.markwat.yuck

/**
  * Description of an installed package
  *
  * @param fqn
  * @param version
  * @param remote
  * @param yuckyData
  */
case class Pkg(fqn: String, version: String, remote: PkgRemote, yuckyData: YuckyData)

object Pkg {

  implicit def asTarget(pkg: Pkg): PkgTarget = PkgTarget(pkg)
}
