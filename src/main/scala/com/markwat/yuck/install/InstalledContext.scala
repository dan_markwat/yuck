package com.markwat.yuck.install

import java.io.File
import java.nio.file.Path

import com.markwat.yuck.PkgData

/**
  * A contextual boundary for operating on a package install.  Elements in
  * this context are used to enforce transition from pre-commit to commit installation
  * phases.
  *
  * @param prefix   a [[File]] which denotes the prefix under which all values under [[manifest]] are mapped to.
  * @param manifest a [[File]] which denotes a directory that maps
  *                 to the root defined by [[prefix]].  Dirs & files under [[manifest]] can
  *                 be shaded or moved to their corresponding destinations under [[prefix]].
  */
case class InstalledContext(prefix: Path, manifest: Seq[Path])

/**
  * The context used for installing [[PkgData]].  This provides a root to operate out of, separate
  * from the actual installation space.  The goal is to keep the filesystem clean of bad installs,
  * similar to how tools like yum perform commits on unpacked RPMs.
  *
  * @param pkgData the source Pkg data
  * @param root    the root to operate out of for performing the install
  */
case class InstallContext(pkgData: PkgData, root: Path)
