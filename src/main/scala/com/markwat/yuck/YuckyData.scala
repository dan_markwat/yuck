package com.markwat.yuck

import java.time.Instant

/**
  * Aka "metadata".
  *
  * @param published    the date of last publication
  * @param buildNumber  the "build number" to distinguish between multiple builds with the same version;
  *                     when 2 Pkgs are being evaluated, they are eventually sorted by this lexicographically
  *                     and the highest-sorted value will win over the others given a version #
  * @param dependencies the [[PkgTarget]]s required by this Pkg to work
  * @param packaging    the packaging used for this Pkg (tar, tar with gzip/bz2, cpio, rpm, etc.)
  */
// todo formally define packaging and allow for multiple values
case class YuckyData(published: Instant, buildNumber: String, dependencies: Seq[PkgTarget], packaging: String)