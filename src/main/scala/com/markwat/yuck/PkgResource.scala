package com.markwat.yuck

import java.net.URI

/**
  * Describes a [[com.markwat.yuck.Pkg]] in the dependency graph.
  *
  * @param fqn
  * @param version
  * @param location
  */
case class PkgResource(fqn: String, version: String, location: URI)

object PkgResource {

  /**
    * Down-"cast" a [[Pkg]] to a [[PkgResource]], as [[PkgResource]] has a proper subset of [[Pkg]]'s properties.
    *
    * @param pkg
    * @return
    */
  def apply(pkg: Pkg): PkgResource = new PkgResource(pkg.fqn, pkg.version, pkg.remote.location)
}
