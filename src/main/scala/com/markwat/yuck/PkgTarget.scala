package com.markwat.yuck

/**
  * Identifies a pkg.
  *
  * @param fqn
  * @param version
  */
case class PkgTarget(fqn: String, version: String)

object PkgTarget {

  def apply(pkg: Pkg): PkgTarget = new PkgTarget(pkg.fqn, pkg.version)

  def apply(pkgResource: PkgResource): PkgTarget = new PkgTarget(pkgResource.fqn, pkgResource.version)
}
