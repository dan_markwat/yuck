package com.markwat.yuck

import java.net.URI

/**
  * Source configuration.  Defines all configurations related to a [[com.markwat.yuck.source.RemoteSource]].
  */
case class SourceConf(location: URI)
