package com.markwat.yuck.layout

import java.net.URI

import com.markwat.yuck.{Pkg, PkgTarget, YuckyData}

/**
  * Defines how a [[com.markwat.yuck.source.RemoteSource]] is laid-out.  [[Pkg]] metadata and
  * [[com.markwat.yuck.PkgData]] are all placed at specific [[URI]]s in the [[com.markwat.yuck.source.RemoteSource]]
  * and repository layouts are defined to translate those objects' information into tangible locations on host
  * systems.
  */
trait RepositoryLayout {
  def path(target: PkgTarget): URI

  def metadata(target: PkgTarget): URI

  def data(pkg: Pkg): URI
}

object RepositoryLayout {

  def apply(base: URI): RepositoryLayout = new DefaultRepositoryLayout(base)
}
