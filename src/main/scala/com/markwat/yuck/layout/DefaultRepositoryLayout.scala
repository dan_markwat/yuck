package com.markwat.yuck.layout

import java.net.URI

import com.markwat.yuck.{Pkg, PkgTarget}

/**
  * Default [[RepositoryLayout]]
  */
class DefaultRepositoryLayout(base: URI) extends RepositoryLayout {
  private[this] def newPath(path: String) = new URI(base.getScheme,
    base.getUserInfo,
    base.getHost,
    base.getPort,
    base.resolve(path).getPath,
    base.getQuery,
    base.getFragment)

  override def path(target: PkgTarget): URI = this.newPath(s"""${target.fqn}/${target.version}/""")

  override def metadata(target: PkgTarget): URI = this.newPath(s"""${target.fqn}/${target.version}/metadata.json""")

  override def data(pkg: Pkg): URI =
    this.newPath(s"""${pkg.fqn}/${pkg.version}/${pkg.fqn}-${pkg.version}.${pkg.yuckyData.packaging}""")
}
