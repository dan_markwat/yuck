package com.markwat.yuck

import java.nio.file._
import java.time.Instant

import com.markwat.yuck.Inventory.Result
import com.markwat.yuck.Inventory.Result.{Failed, Succeeded}
import com.markwat.yuck.action.Installer
import com.markwat.yuck.install.{InstallContext, InstalledContext}
import com.markwat.yuck.serde.{Deser, Ser}
import com.sun.nio.file.ExtendedOpenOption.NOSHARE_WRITE
import org.apache.commons.io.IOUtils

import scala.collection.JavaConverters._
import scala.util.Try

/**
  * The local state of what's installed, available, etc.
  */
trait Inventory {

  /**
    * Get all the manifests this inventory is responsible for.
    *
    * @return
    */
  def manifests: Seq[PkgManifest]

  /**
    * Tells us if a Pkg matching the target criteria is installed.
    *
    * @param target the [[PkgTarget]] criteria we are checking
    * @return true if installed, false otherwise
    */
  def isInstalled(target: PkgTarget): Boolean

  /**
    * Wraps a [[PkgResource]]'s installation in a contained function for the Inventory to
    * manage.  The installer performs the installation of the [[PkgData]] and returns
    * an [[InstalledContext]] for the Inventory to either commit or revert.
    *
    * @param pkgData   the [[PkgData]] to install
    * @param installer the [[Installer]] to install the [[Pkg]]
    * @return
    */
  // todo decide if it's the Inventory's responsibility to manage commit at the low level
  def install(pkgData: PkgData)(installer: Installer): Result

  def remove(pkg: PkgTarget): Unit
}

object Inventory {

  trait Result

  object Result {

    case object Succeeded extends Result

    case object Failed extends Result

  }

  class InventoryImpl(invSite: Path)(implicit ser: Ser[PkgManifest], deser: Deser[PkgManifest]) extends Inventory {

    /**
      * Perform move/merge of all dirs/files from the [[InstalledContext]] to
      * their final destinations
      *
      * @param instCtx
      */
    // todo change to check before do?  or support rollback?
    def commit(pkg: Pkg, instCtx: InstalledContext): Unit = {
      val rootFs = Paths.get("/")

      val relatives = instCtx.manifest.map(instCtx.prefix.relativize)

      relatives.foreach { rel =>
        val relativized = rootFs.resolve(rel)
        val entry = instCtx.prefix.resolve(rel)

        // entry is dir
        if (Files.isDirectory(entry)) {
          // relativized path doesn't exist
          if (Files.notExists(relativized)) {
            // create it
            Files.createDirectories(relativized)
          }
          // relativized path exists
          else {
            // do nothing? move could be problematic when we process new files if we do
            // todo make processing dir moves more elegant?
          }
        }
        // entry is file
        else {
          // relativized path doesn't exist
          if (Files.notExists(relativized)) {
            // move it
            Files.move(entry, relativized)
          }
          // relativized path exists
          else {
            // todo should we throw a better exception?
            throw new FileAlreadyExistsException(relativized.toString, entry.toString, "Can't move yuck-managed file")
          }
        }
      }

      val invEntry = invSite.resolve(s"${pkg.fqn}-${pkg.version}.yuck")

      // write entry into yuck site
      val manifest = PkgManifest(pkg, Instant.now(), relatives.map(rootFs.resolve(_).toString))
      // put in file
      resource.managed(ser apply manifest).map({ manifestStream =>
        resource.managed(Files.newOutputStream(invEntry, NOSHARE_WRITE)).map({ manifestFileOutStream =>
          IOUtils.copy(manifestStream, manifestFileOutStream)
        }).tried.get
      }).tried.get
    }

    override def isInstalled(target: PkgTarget): Boolean =
      manifests.map(manifest => PkgTarget(manifest.target)).contains(target)

    override def install(pkgData: PkgData)(installer: (InstallContext) => InstalledContext): Result = {
      // try to run the installer & perform commit
      val tried = Try {
        val root = Files.createTempDirectory("yuck-install")
        val installed = installer.apply(InstallContext(pkgData, root))
        this.commit(pkgData.pkg, installed)
      }

      tried
        // map to Succeeded
        .map(_ => Succeeded)
        // get or else return Failed
        .getOrElse(Failed)
    }

    override def remove(pkg: PkgTarget): Unit = ???

    override def manifests: Seq[PkgManifest] = Files.list(invSite).iterator().asScala.toVector.map(pkg =>
      resource.managed(Files.newInputStream(pkg)).map(is => deser.apply(is)).tried.get)
  }

}
