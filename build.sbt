
lazy val yuck = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    name := "yuck",
    version := "1.0",
    organization := "com.markwat",
    scalaVersion := "2.12.2",
    Defaults.itSettings)
  .settings(
    scalacOptions += "-deprecation",
    scalacOptions += "-feature",
    scalacOptions += "-language:implicitConversions")
  .settings(
    libraryDependencies ++= "com.github.scopt" %% "scopt" % "3.5.0" ::
      "org.apache.commons" % "commons-compress" % "1.14" ::

      "com.softwaremill.macwire" %% "macros" % "2.3.0" ::
      "com.softwaremill.macwire" %% "util" % "2.3.0" ::
      "com.softwaremill.macwire" %% "proxy" % "2.3.0" ::

      "com.typesafe" % "config" % "1.3.1" ::
      "org.json4s" %% "json4s-native" % "3.5.2" ::
      "org.json4s" %% "json4s-ext" % "3.5.2" ::
      "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0" ::
      "ch.qos.logback" % "logback-classic" % "1.2.3" ::
      "com.jsuereth" %% "scala-arm" % "2.0" ::

      "org.apache.jena" % "jena-arq" % "3.3.0" ::
      "org.apache.jena" % "jena-tdb" % "3.3.0" ::

      //"org.scalaj" %% "scalaj-http" % "2.3.0" ::
      "org.http4s" %% "http4s-blaze-client" % "0.17.0-M2" ::
      Nil)
  .settings(
    libraryDependencies ++= "org.scalactic" %% "scalactic" % "3.0.1" ::
      "org.scalatest" %% "scalatest" % "3.0.1" % Test ::

      "org.scalamock" %% "scalamock-core" % "3.6.0" % Test ::
      "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % Test ::

      "org.mock-server" % "mockserver-netty" % "3.10.7" % Test ::
      Nil)
  .settings(
    libraryDependencies ++= "org.scalatest" %% "scalatest" % "3.0.1" % IntegrationTest ::

      "org.scalamock" %% "scalamock-core" % "3.6.0" % IntegrationTest ::
      "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % IntegrationTest ::

      "org.mock-server" % "mockserver-netty" % "3.10.7" % IntegrationTest ::
      Nil)
